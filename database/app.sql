SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

INSERT INTO `adverts` (`id`, `author_id`, `category_id`, `region_id`, `type`, `title`, `description`, `address`, `views`, `slug`, `status`, `created_at`, `updated_at`, `author_name`, `author_phone`, `author_email`) VALUES
(2,	2,	2,	1,	'donate',	'Спинер синий',	'Все работает на отлично. Есть царапины.',	'Варненская 44',	6,	'spiner-siniy',	'active',	'2018-11-21 12:48:03',	'2018-11-21 12:58:48',	NULL,	NULL,	NULL),
(3,	2,	4,	1,	'donate',	'Часы Casio',	'Новые недорого',	'Варненская 44',	2,	'chasy-casio',	'active',	'2018-11-21 14:07:54',	'2018-11-21 14:09:34',	NULL,	NULL,	NULL),
(4,	2,	4,	1,	'donate',	'Отдам Casio',	'Пользовался очень бережно',	'Варненская !7/6',	0,	'otdam-casio',	'active',	'2018-11-21 14:13:29',	'2018-11-21 14:13:29',	NULL,	NULL,	NULL),
(5,	2,	4,	1,	'donate',	'Casio Ediface',	'Casio Ediface',	'Варненская 17/6',	3,	'casio-ediface',	'active',	'2018-11-21 14:20:42',	'2018-11-21 14:39:47',	NULL,	NULL,	NULL),
(6,	2,	4,	1,	'donate',	'Какие то часы',	'Нашел в чулане',	'Маршала Жукова 1',	1,	'kakie-to-chasy',	'active',	'2018-11-21 14:23:18',	'2018-11-21 14:23:27',	NULL,	NULL,	NULL),
(7,	2,	7,	2,	'donate',	'Отличные штиблеты',	'Отличные штиблеты',	'Варненская 41',	0,	'otlichnye-shtiblety',	'active',	'2018-11-21 14:25:38',	'2018-11-21 14:25:38',	NULL,	NULL,	NULL),
(8,	2,	7,	3,	'donate',	'Почти новые',	'Почти новые',	'Варненская 4',	0,	'pochti-novye',	'active',	'2018-11-21 14:26:46',	'2018-11-21 14:26:46',	NULL,	NULL,	NULL),
(9,	2,	7,	4,	'donate',	'Классные и 45 размера',	'Классные и 45 размера',	'Варненская 44',	0,	'klassnye-i-45-razmera',	'active',	'2018-11-21 14:28:20',	'2018-11-21 14:28:20',	NULL,	NULL,	NULL),
(10,	2,	7,	1,	'donate',	'Кому надо?',	'новые',	'Варненская 44',	0,	'komu-nado',	'active',	'2018-11-21 14:29:54',	'2018-11-21 14:33:20',	NULL,	NULL,	NULL),
(11,	2,	4,	1,	'receive',	'Приму в дар ферари',	'Очень прошу- не будьде равнодушны',	'Варненская 4',	4,	'primu-v-dar-ferari',	'active',	'2018-11-21 17:27:38',	'2018-11-21 17:34:30',	NULL,	NULL,	NULL),
(12,	2,	2,	1,	'donate',	'Последняя модель спинера',	'круть',	'Варненская 2',	0,	'poslednyaya-model-spinera',	'active',	'2018-11-21 19:34:44',	'2018-11-21 19:34:44',	NULL,	NULL,	NULL);

INSERT INTO `advert_photos` (`id`, `created_at`, `updated_at`, `advert_id`, `title`) VALUES
(2,	'2018-11-21 12:48:03',	'2018-11-21 12:48:03',	2,	'dZaU5oiIKIVnZTMgtQJXrkIhqE2fqdDEzYIwWgqN.jpeg'),
(3,	'2018-11-21 14:07:55',	'2018-11-21 14:07:55',	3,	'IZ2d3icxIQpWpHpMI2wTN5MhHKLgL8ty2AAyAvfn.jpeg'),
(4,	'2018-11-21 14:13:29',	'2018-11-21 14:13:29',	4,	'l1uHTEr3wbQyF97QoKu2XDuFQlOBJENOCfrulMRb.jpeg'),
(5,	'2018-11-21 14:20:42',	'2018-11-21 14:20:42',	5,	'OPpvWcxxPcXG0O9EwKfWhkNWvcsTwwYQuK1UofiF.jpeg'),
(6,	'2018-11-21 14:23:18',	'2018-11-21 14:23:18',	6,	'blnM1XPciuYh2I7Qky6u4YLxIO9lOuKwQ0TQtlJo.png'),
(7,	'2018-11-21 14:25:38',	'2018-11-21 14:25:38',	7,	'uv57JqoTKtx0O9dVhVHKIwylPK68Vrif1HWsg9eC.jpeg'),
(8,	'2018-11-21 14:26:46',	'2018-11-21 14:26:46',	8,	'CqvnU3uIeNtjEJOeEjcYdlfaDfoPJLCDjkf3CyGb.jpeg'),
(9,	'2018-11-21 14:28:20',	'2018-11-21 14:28:20',	9,	'A4NKUS5JHqtkv390m7qAi2Zt1ZXb1Sdh4rbVApZk.jpeg'),
(10,	'2018-11-21 14:29:54',	'2018-11-21 14:29:54',	10,	'95uzwE5RJRbuyPPpNtAAAuiYHzswAHKgqf8H8jNZ.jpeg');

INSERT INTO `categories` (`id`, `title`, `slug`, `seo_title`, `seo_description`, `seo_text`, `parent_id`, `created_at`, `updated_at`) VALUES
(1,	'Гаджеты',	'gadzhety',	NULL,	NULL,	NULL,	NULL,	'2018-11-21 12:08:42',	'2018-11-21 12:08:42'),
(2,	'Спинеры',	'spinery',	NULL,	NULL,	NULL,	1,	'2018-11-21 12:09:00',	'2018-11-21 12:09:00'),
(3,	'Телефоны',	'telefony',	NULL,	NULL,	NULL,	1,	'2018-11-21 12:09:17',	'2018-11-21 12:09:17'),
(4,	'Умные часы',	'umnye-chasy',	NULL,	NULL,	NULL,	1,	'2018-11-21 12:09:29',	'2018-11-21 12:09:29'),
(5,	'Вещи',	'veshchi',	NULL,	NULL,	NULL,	NULL,	'2018-11-21 12:09:49',	'2018-11-21 12:09:49'),
(6,	'Пиджаки',	'pidzhaki',	NULL,	NULL,	NULL,	5,	'2018-11-21 12:10:00',	'2018-11-21 12:10:00'),
(7,	'Обувь',	'obuv',	NULL,	NULL,	NULL,	5,	'2018-11-21 12:10:17',	'2018-11-21 12:10:17'),
(8,	'Инструменты',	'instrumenty',	NULL,	NULL,	NULL,	NULL,	'2018-11-21 12:10:46',	'2018-11-21 12:10:46'),
(9,	'Гитары',	'gitary',	NULL,	NULL,	NULL,	8,	'2018-11-21 12:10:59',	'2018-11-21 12:10:59'),
(10,	'Барабаны',	'barabany',	NULL,	NULL,	NULL,	8,	'2018-11-21 12:11:18',	'2018-11-21 12:11:18');

INSERT INTO `favorites` (`id`, `created_at`, `updated_at`, `advert_id`, `user_id`) VALUES
(1,	'2018-11-21 14:48:00',	'2018-11-21 14:48:00',	8,	2),
(2,	'2018-11-21 14:48:03',	'2018-11-21 14:48:03',	10,	2);

INSERT INTO `regions` (`id`, `title`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1,	'Одесса',	'odessa',	'active',	'2018-11-21 12:07:13',	'2018-11-21 12:07:13'),
(2,	'Киев',	'kiev',	'active',	'2018-11-21 12:07:26',	'2018-11-21 12:07:26'),
(3,	'Крыжополь',	'kryzhopol',	'active',	'2018-11-21 12:07:36',	'2018-11-21 12:07:36'),
(4,	'Чернобыль',	'chernobyl',	'active',	'2018-11-21 12:07:50',	'2018-11-21 12:07:50'),
(5,	'Харьков',	'harkov',	'active',	'2018-11-21 12:07:59',	'2018-11-21 12:07:59'),
(6,	'Львов',	'lvov',	'active',	'2018-11-21 12:08:19',	'2018-11-21 12:08:19');

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `region_id`, `status`, `is_admin`, `phone`) VALUES
(2,	'default',	'nyah64@example.org',	'$2y$10$oh4E8qHbeTOnGWpbSnyi6eJUSEQOcyD0nFfRKVjsNfkQNTjTotEBS',	NULL,	'2018-11-21 12:25:24',	'2018-11-21 12:25:24',	1,	'active',	0,	'30638755333');
