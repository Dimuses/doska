<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdvertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adverts', function (Blueprint $table) {
            $table->dropColumn('author_name');
            $table->dropColumn('author_phone');
            $table->dropColumn('author_email');
        });
        Schema::table('adverts', function (Blueprint $table) {
            $table->string('author_name', 32)->nullable();
            $table->string('author_phone', 15)->nullable();
            $table->string('author_email', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
