<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('adverts')) {
            Schema::create('adverts', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('author_id')->unsigned();
                $table->string('author_name', 255);
                $table->string('author_email', 255);
                $table->integer('category_id')->unsigned();
                $table->integer('region_id')->unsigned();
                $table->enum('type', ['donate', 'receive']);
                $table->string('title', 255);
                $table->text('description');

                $table->string('address', 255);
                $table->integer('views')->default(0);
                $table->string('slug');
                $table->enum('status', ['active', 'inactive', 'blocked']);
                $table->timestamps();

                $table->index('category_id');
                $table->index('region_id');
                $table->index('title');
                $table->index('views');
                $table->index('status');
                $table->index('slug');

                $table->foreign('author_id')->references('id')->on('users');
                $table->foreign('region_id')->references('id')->on('regions');
                $table->foreign('category_id')->references('id')->on('categories');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('adverts');
        Schema::enableForeignKeyConstraints();
    }
}
