<?php

use Illuminate\Database\Migrations\Migration;

class AddAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert(
            [
                'email' => 'john@example.com',
                'username'  => 'username',
                'password'  => bcrypt('secret'),
                'region_id' => 0,
                'status'    => 'active',
                'is_admin'  => 1,
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
