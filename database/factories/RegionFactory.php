<?php

use Faker\Generator as Faker;

$factory->define(App\Models\common\Region::class, function (Faker $faker) {

    $statuses = ['active', 'inactive'];
    return [
        'title' => $faker->state,
        'status' => $statuses[array_rand($statuses, 1)],
    ];
});
