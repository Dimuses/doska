<?php

use Faker\Generator as Faker;

$factory->define(App\Models\common\Advert::class, function (Faker $faker) {
    $statuses = ['active', 'inactive'];
    $types = \App\Models\common\Advert::getTypes();
    return [
        'title' => $faker->title,
        'author_id' => $faker->randomElement(\App\Models\common\User::all()->keyBy('id')->all()),
        'status' => $faker->randomElement($statuses),
        'category_id' => $faker->randomElement(\App\Models\common\Category::all()->keyBy('id')->all()),
        'region_id' => $faker->randomElement(\App\Models\common\Region::all()->keyBy('id')->all()),
        'author_phone' => $faker->phoneNumber,
        'description' => $faker->paragraph,
        'address' => $faker->streetAddress,
        'type' => $faker->randomElement($types)
    ];
});
