<?php

namespace App\Policies;

use App\Models\common\AdvertPhoto;
use App\Models\common\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class AdvertPhotoPolicy
 *
 * @package App\Policies
 *
 * @author Dimus <iron@te.net.ua>
 */
class AdvertPhotoPolicy
{
    use HandlesAuthorization;

    /**
     * Can delete photo from advert
     *
     * @param User        $user  instance
     * @param AdvertPhoto $photo instance
     *
     * @return bool
     */
    public function deletePhoto($user, AdvertPhoto $photo)
    {
        return $user->id === $photo->advert->author_id;
    }
}