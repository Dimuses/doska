<?php

namespace App\Policies;

use App\Models\common\Advert;
use App\Models\common\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class AdvertPolicy
 *
 * @package App\Policies
 * @author  Dimus <iron@te.net.ua>
 */
class AdvertPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the advert.
     *
     * @param User   $user   instance
     * @param Advert $advert instance
     *
     * @return mixed
     */
    public function update(User $user, Advert $advert)
    {
        return $user->id === $advert->author_id;
    }

    /**
     * Determine whether the user can delete the advert.
     *
     * @param User   $user   instance
     * @param Advert $advert instance
     *
     * @return mixed
     */
    public function delete(User $user, Advert $advert)
    {
        return $user->id === $advert->author_id;
    }
}
