<?php

namespace App\Http\Requests;

use App\Models\common\Advert;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AdvertUserCreateRequest
 *
 * @package App\Http\Requests
 * @author  Dimus <iron@te.net.ua>
 */
class AdvertUserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|string|max:255',
            'category_id'  => 'required|integer|exists:categories,id',
            'region_id'    => 'required|integer|exists:regions,id',
            'author_phone' => 'required|integer',
            'images.*'     => 'image',
            'type'         => [
                'required',
                'string',
                Rule::in(
                    Advert::TYPE_DONATE,
                    Advert::TYPE_RECEIVE
                ),
            ],
            'description'  => 'string',
            'status'       => [
                'required',
                'string',
                Rule::in(
                    Advert::STATUS_ACTIVE,
                    Advert::STATUS_INACTIVE
                ),
            ],
            'author_name'  => 'nullable|string|max:255',
            'author_email' => 'nullable|string|max:255',
            'address'      => 'required|max:255|string',
        ];
    }
}
