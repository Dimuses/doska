<?php

namespace App\Http\Requests;

use App\Models\common\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UserUpdateRequest
 *
 * @package App\Http\Requests
 * @author  Dimus <iron@te.net.ua>
 */
class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'  => 'required|max:255|string|unique:users,id,'
                . $this->user->id,
            'email'     => 'required|email|unique:users,email,'
                . $this->user->id,
            'status'    => [
                'required',
                'string',
                Rule::in(User::STATUS_ACTIVE, User::STATUS_INACTIVE),
            ],
            'region_id' => 'required|integer|exists:regions,id',
            'password'  => 'string|max:60|nullable',
            'phone'     => 'string|max:15',
        ];
    }
}
