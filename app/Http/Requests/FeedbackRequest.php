<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FeedbackRequest
 *
 * @package App\Http\Requests
 * @author  Dimus <iron@te.net.ua>
 */
class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'message' => 'required|string',
        ];
    }
}
