<?php

namespace App\Http\Requests;

use App\Models\common\Region;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class RegionCreateRequest
 *
 * @package App\Http\Requests
 * @author  Dimus <iron@te.net.ua>
 */
class RegionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255|string|unique:regions',
            'status' => [
                'required',
                'string',
                Rule::in(Region::STATUS_ACTIVE, Region::STATUS_INACTIVE),
            ],
        ];
    }
}
