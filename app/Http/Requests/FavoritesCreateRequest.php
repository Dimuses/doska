<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FavoritesCreateRequest
 *
 * @package App\Http\Controllers\Frontend
 * @author  Dimus <iron@te.net.ua>
 */
class FavoritesCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'advert_id' => 'required|integer|exists:adverts,id',
        ];
    }
}