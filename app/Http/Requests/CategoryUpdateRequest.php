<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CategoryUpdateRequest
 *
 * @package App\Http\Requests
 * @author  Dimus <iron@te.net.ua>
 */
class CategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|max:255|string|unique:categories,title,'
                . $this->category->id,
            'seo_title' => 'nullable|string|max:255',
            'seo_text'  => 'nullable|string',
            'parent_id' => 'nullable|exists:categories,id',
        ];
    }
}
