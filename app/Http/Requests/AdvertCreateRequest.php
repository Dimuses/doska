<?php

namespace App\Http\Requests;

use App\Models\common\Advert;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class AdvertCreateRequest
 *
 * @package App\Http\Requests
 * @author  Dimus <iron@te.net.ua>
 */
class AdvertCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required|string|max:255',
            'author_id'    => 'required|integer|exists:users,id',
            'category_id'  => 'required|integer|exists:categories,id',
            'region_id'    => 'required|integer|exists:regions,id',
            'author_phone' => 'string|max:15|nullable',
            'images.*'     => 'image',
            'type'         => [
                'required',
                'string',
                Rule::in(
                    Advert::TYPE_DONATE,
                    Advert::TYPE_RECEIVE
                ),
            ],
            'description'  => 'string',
            'status'       => [
                'required',
                'string',
                Rule::in(
                    Advert::STATUS_ACTIVE,
                    Advert::STATUS_INACTIVE,
                    Advert::STATUS_BLOCKED
                ),
            ],
            'author_name'  => 'nullable|string|max:32|nullable',
            'author_email' => 'nullable|string|max:32|nullable',
            'address'      => 'required|max:255|string',
        ];
    }
}
