<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegionCreateRequest;
use App\Http\Requests\RegionUpdateRequest;
use App\Http\Services\RegionService;
use App\Models\common\Region;
use Illuminate\Http\Request;

/**
 * Class RegionController
 *
 * @package App\Http\Controllers\Admin
 * @author  Dimus <iron@te.net.ua>
 */
class RegionController extends Controller
{
    /**
     * Region service instance
     *
     * @var \App\Http\Services\RegionService
     */
    private $_regionService;

    /**
     * RegionController constructor.
     *
     * @param RegionService $regionService instance
     */
    public function __construct(RegionService $regionService)
    {
        $this->_regionService = $regionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $regions = Region::search($request, true, 15);
        $statuses = $this->_regionService->getAllStatuses();
        return view('admin.regions.index', compact('statuses', 'regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = $this->_regionService->getAllStatuses();
        return view('admin.regions.create', compact('statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RegionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RegionCreateRequest $request)
    {
        Region::create(
            [
                'status' => $request->status,
                'title' => $request->title,
            ]
        );
        return redirect()->route('admin.regions.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.regions.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Region $region
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Region $region)
    {
        $statuses = $this->_regionService->getAllStatuses();
        return view('admin.regions.edit', compact('region', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RegionUpdateRequest $request
     * @param Region              $region
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RegionUpdateRequest $request, Region $region)
    {
        $region->update($request->all());
        return redirect()->route('admin.regions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Region  $region
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region, Request $request)
    {
        try {
            $region->delete();
        } catch (\Throwable $e) {
            $request->session()->flash('region_admin', 'Unsuccessful deleting!');
        }
        return redirect()->route('admin.regions.index');
    }
}
