<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdvertCreateRequest;
use App\Http\Requests\AdvertUpdateRequest;
use App\Http\Services\AdvertService;
use App\Http\Services\CategoryService;
use App\Http\Services\RegionService;
use App\Models\common\Advert;
use App\Models\common\User;
use Illuminate\Http\Request;

/**
 * Class AdvertController
 *
 * @package App\Http\Controllers\Admin
 * @author  Dimus <iron@te.net.ua>
 */
class AdvertController extends Controller
{
    /**
     * Advert service instance
     *
     * @var AdvertService
     */
    private $_advertService;
    /**
     * Region service instance
     *
     * @var RegionService
     */
    private $_regionService;
    /**
     * CategoryService instance
     *
     * @var CategoryService
     */
    private $_categoryService;

    /**
     * RegionController constructor.
     *
     * @param AdvertService   $advertService   instance
     * @param RegionService   $regionService   instance
     * @param CategoryService $categoryService instance
     */
    public function __construct(
        AdvertService $advertService,
        RegionService $regionService,
        CategoryService $categoryService
    ) {
        $this->_advertService = $advertService;
        $this->_regionService = $regionService;
        $this->_categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request instance
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $adverts = Advert::search($request, true, 15);
        $regions = $this->_regionService->getAll();
        $categories = $this->_categoryService->getAll();
        $statuses = $this->_advertService->getAllStatuses();
        return view(
            'admin.adverts.index',
            compact(
                'adverts',
                'regions',
                'categories',
                'statuses'
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $statuses = $this->_advertService->getAllStatuses();
        $regions = $this->_regionService->getAll();
        $categories = $this->_categoryService->getSubCategories();
        $types = $this->_advertService->getAllTypes();
        $users = array_pluck(User::all(), 'username', 'id');

        return view(
            'admin.adverts.create',
            compact(
                'statuses',
                'regions',
                'categories',
                'types',
                'users'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdvertCreateRequest $request instance
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertCreateRequest $request)
    {
        try {
            $this->_advertService->saveWithPhoto(
                Advert::make(),
                $request->all(),
                array_get($request, 'images', [])
            );
        } catch (\Throwable $e) {
            $request->session()->flash('advert', 'Error storing');
        }
        return redirect()->route('admin.adverts.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Advert $advert instance
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Advert $advert)
    {
        $statuses = $this->_advertService->getAllStatuses();
        $regions = $this->_regionService->getAll();
        $categories = $this->_categoryService->getSubCategories();
        $types = $this->_advertService->getAllTypes();
        $users = array_pluck(User::all(), 'username', 'id');

        $data = $this->_advertService->prepareGalleryData($advert);
        $initialPreview = array_get($data, 'initialPreview');
        $initialPreviewConfig = array_get($data, 'initialPreviewConfig');

        return view(
            'admin.adverts.edit',
            compact(
                'advert',
                'statuses',
                'regions',
                'categories',
                'types',
                'users',
                'initialPreview',
                'initialPreviewConfig'
            )
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdvertUpdateRequest $request instance
     * @param Advert              $advert  instance
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertUpdateRequest $request, Advert $advert)
    {
        try {
            $this->_advertService->saveWithPhoto(
                $advert,
                $request->all(),
                array_get($request, 'images', [])
            );
        } catch (\Throwable $e) {
            $request->session()->flash('advert', 'Error updating');
        }
        return redirect()->route('admin.adverts.index');
    }

    /**
     * Destroy photo by ajax
     *
     * @param Request $request instance
     *
     * @return array
     */
    public function destroyPhoto(Request $request)
    {
        $photoId = $request->get('id', null);
        if ($this->_advertService->deletePhoto($photoId)) {
            return [];
        }
        return ['error' => 'fail'];
    }

    /**
     * Destroy action
     *
     * @param Advert  $advert  instance
     * @param Request $request instance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Advert $advert, Request $request)
    {
        try {
            $this->_advertService->deleteAdvert($advert);
        } catch (\Throwable $e) {
            $request->session()->flash('advert', 'Unsuccessful deleting!');
        }
        return redirect()->route('admin.adverts.index');
    }
}
