<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Services\RegionService;
use App\Http\Services\UserService;
use App\Models\common\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Admin
 * @author  Dimus <iron@te.net.ua>
 */
class UserController extends Controller
{

    /**
     * User service instance
     *
     * @var \App\Http\Services\UserService
     */
    private $_userService;
    /**
     * Region service instance
     *
     * @var \App\Http\Services\RegionService
     */
    private $_regionService;

    /**
     * UserController constructor.
     *
     * @param UserService   $userService   instance
     * @param RegionService $regionService instance
     */
    public function __construct(
        UserService $userService,
        RegionService $regionService
    ) {
        $this->_userService = $userService;
        $this->_regionService = $regionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::search($request, true, 15);
        $statuses = $this->_userService->getAllStatuses();
        $regions = $this->_regionService->getAll();
        return view('admin.users.index', compact('users', 'statuses', 'regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = $this->_regionService->getAll();
        $statuses = $this->_userService->getAllStatuses();
        return view('admin.users.create', compact('regions', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $this->_userService->create($request->all());
        return redirect()->route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.users.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\common\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $regions = $this->_regionService->getAll();
        $statuses = $this->_userService->getAllStatuses();
        return view('admin.users.edit', compact('regions', 'statuses', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest       $request
     * @param \App\Models\common\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        if ($request->input('password')) {
            $request->password = User::passwordHash($request->password);
            $data = $request->all();
        } else {
            $data = $request->except('password');
        }
        $user->update($data);
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\common\User $user
     * @param Request                 $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Request $request)
    {
        try {
            $user->delete();
        } catch (\Throwable $e) {
            $request->session()->flash('user', 'Unsuccessful deleting!');
        }
        return redirect()->route('admin.users.index');
    }

    /**
     * Find action
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Models\common\User[]|Collection|\Illuminate\Support\Collection
     */
    public function find(Request $request)
    {
        $query = $request->input('q');
        $results = $this->_userService->findByName($query);
        $results = $results->map(
            function ($item) {
                return ['id' => $item->id, 'text' => $item->username];
            }
        );
        return $results;
    }
}
