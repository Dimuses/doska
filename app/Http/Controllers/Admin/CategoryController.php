<?php

namespace App\Http\Controllers\Admin;

use App\Models\common\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryUpdateRequest;

use App\Http\Services\CategoryService;

use Illuminate\Http\Request;

/**
 * Class CategoryController
 *
 * @package App\Http\Controllers\Admin
 * @author  Dimus <iron@te.net.ua>
 */
class CategoryController extends Controller
{
    /**
     * Category service instance
     * @var \App\Http\Services\CategoryService
     */
    private $_categoryService;

    /**
     * RegionController constructor.
     *
     * @param CategoryService $categoryService instance
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->_categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $parents = $this->_categoryService->getTopCategories();
        $categories = Category::search($request, true, 15);
        return view('admin.categories.index', compact('categories', 'parents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parents = $this->_categoryService->getTopCategories();
        return view('admin.categories.create', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreateRequest $request)
    {
        Category::create(
            [
                'title' => $request->title,
                'parent_id' => $request->parent_id,
            ]
        );
        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.categories.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $parents = $this->_categoryService->getTopCategories($category->id);
        return view('admin.categories.edit', compact('parents', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryUpdateRequest $request
     * @param Category              $category
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        $category->slug = null;
        $category->update($request->all());
        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @param Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, Request $request)
    {
        try {
            $category->delete();
        } catch (\Exception $e) {
            $request->session()->flash('category', 'Unsuccessful deleting!');
        }
        return redirect()->route('admin.categories.index');
    }
}
