<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\FeedbackRequest;
use App\Mail\Feedback;
use App\Models\common\Advert;
use App\Http\Controllers\Controller;
use App\Http\Services\AdvertService;
use App\Http\Services\CategoryService;
use App\Http\Services\RegionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/**
 * Site controller
 *
 * @author Dimus <iron@te.net.ua>
 */
class SiteController extends Controller
{
    /**
     * Region service instance
     *
     * @var RegionService
     */
    private $_regionService;
    /**
     * Advert service instance
     *
     * @var AdvertService
     */
    private $_advertService;

    /**
     * Category service instance
     *
     * @var CategoryService
     */
    private $_categoryService;

    /**
     * SiteController constructor.
     *
     * @param RegionService   $regionService   instance
     * @param AdvertService   $advertService   instance
     * @param CategoryService $categoryService instance
     */
    public function __construct(
        RegionService $regionService,
        AdvertService $advertService,
        CategoryService $categoryService
    ) {
        $this->_regionService = $regionService;
        $this->_advertService = $advertService;
        $this->_categoryService = $categoryService;
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function index()
    {
        $regionId = (int)\session()->get('region');
        if ($regionId) {
            $region = $this->_regionService->get($regionId);
        }
        $categoriesCollection = $this->_categoryService->getAll();
        $adverts = $this->_advertService->getAllActive(
            Advert::TYPE_DONATE, $regionId
        );
        $showRegion = ($regionId === 0);
        $region = $region ?? null;
        $type = Advert::TYPE_DONATE;
        $textBlock = '';

        return view(
            'frontend.site.index',
            compact(
                'region',
                'categoriesCollection',
                'adverts',
                'showRegion',
                'type',
                'textBlock'
            )
        );
    }

    /**
     * Set region from navbar
     *
     * @param Request $request instance
     *
     * @return void
     */
    public function session(Request $request)
    {
        if ($request->ajax()) {
            $request->input('regionId');
            session(['region' => $request->input('regionId')]);
        }
    }

    /**
     * Show feedback form action
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function feedback()
    {
        return view('frontend.site.feedback');
    }

    /**
     * Send feedback data action
     *
     * @param \App\Http\Requests\FeedbackRequest $request instance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendFeedback(FeedbackRequest $request)
    {
        Mail::to(
            config('supportEmail', 'test@test.ru'),
            new Feedback(
                array_get($request, 'name'),
                array_get($request, 'email'),
                array_get($request, 'message')
            )
        );
        session()->flash('feedback', 'Ваше сообщение было успешно отправлено');
        return redirect()->refresh();
    }
}
