<?php

namespace App\Http\Controllers\Frontend;

use App\Models\frontend\Advert;
use App\Http\Controllers\Controller;
use App\Http\Services\AdvertService;
use App\Http\Services\CategoryService;
use App\Http\Services\RegionService;
use App\Models\frontend\Category;
use Auth;
use Illuminate\Http\Request;

/**
 * Class AdvertController
 *
 * @package App\Http\Controllers\Frontend
 * @author  Dimus <iron@te.net.ua>
 */
class AdvertController extends Controller
{
    /**
     * Advert service instance
     * @var \App\Http\Services\AdvertService
     */
    private $_advertService;
    /**
     * Region service instance
     * @var \App\Http\Services\RegionService
     */
    private $_regionService;
    /**
     * Category service instance
     * @var \App\Http\Services\CategoryService
     */
    private $_categoryService;
    /**
     * Favorites service instance
     * @var \App\Http\Controllers\Frontend\FavoritesService
     */
    private $_favoriteService;

    /**
     * RegionController constructor.
     *
     * @param AdvertService    $advertService   instance
     * @param RegionService    $regionService   instance
     * @param FavoritesService $favoriteService instance
     * @param CategoryService  $categoryService instance
     */
    public function __construct(
        AdvertService $advertService,
        RegionService $regionService,
        FavoritesService $favoriteService,
        CategoryService $categoryService
    ) {
        $this->_advertService = $advertService;
        $this->_regionService = $regionService;
        $this->_categoryService = $categoryService;
        $this->_favoriteService = $favoriteService;
    }

    /**
     * Index action
     *
     * @param string                             $type
     * @param \App\Models\frontend\Category|null $category
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index($type, ?Category $category)
    {
        $regionId = (int)session()->get('region');
        if ($regionId) {
            $region = $this->_regionService->get($regionId);
        }
        $adverts = $this->_advertService->getAllActive(
            $type,
            $regionId,
            $category->id ?? null
        );
        return view(
            'frontend.advert.index',
            [
                'region' => $region ?? null,
                'categoriesCollection' => $this->_categoryService->getAll(),
                'showRegion' => ($regionId === 0),
                'type' => $type,
                'category' => $category ?? null,
                'adverts' => $adverts,
                'seo' => [
                    'title' => isset($category)
                        ? $category->seo_title
                        : null,
                    'description' => isset($category)
                        ? $category->seo_description
                        : null,
                    'text' => isset($category)
                        ? $category->seo_text
                        : null,
                ],
            ]
        );
    }

    /**
     * Show action
     *
     * @param \App\Models\frontend\Advert $advert
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function show(Advert $advert)
    {
        $coords = [];
        $regionId = (int)session()->get('region');
        $query = $advert->region->title . '' . $advert->address;
        $data = \YaGeo::setQuery($query)->load();
        if ($data->getResponse()) {
            $coords[] = $data->getResponse()->getLatitude();
            $coords[] = $data->getResponse()->getLongitude();
        }
        $this->_advertService->updateViews($advert->id, 1);

        try {
            $region = $regionId
                ? $this->_regionService->get($regionId)
                : null;
        } catch (\Throwable $e) {
            session()->flash('advert', 'Region not found');
        }
        $view = $advert->type == Advert::TYPE_DONATE
            ? 'frontend.advert.view'
            : 'frontend.advert.view_receive';
        $categoriesCollection = $this->_categoryService->getAll();
        $canClose = false;
        //$canClose = $user->can('update', $advert);
        $region = $region ?? null;
        $related = $this->_advertService->getRelated($advert);
        $isFavorite = $this->_favoriteService->isFavorite($advert->id, Auth::id());

        return view(
            $view,
            compact(
                'advert',
                'region',
                'categoriesCollection',
                'canClose',
                'coords',
                'related',
                'isFavorite'
            )
        );
    }

    /**
     * Search action
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function search(Request $request)
    {
        $regionId = (int)session()->get('region');
        $categoryId = array_get($request, 'category');
        $query = array_get($request, 'q');
        $showRegion = ($regionId === 0);
        $adverts = $this->_advertService->search(
            [
                'region_id' => $regionId,
                'category_id' => $categoryId,
                'query' => $query,
            ]
        );
        return view(
            'frontend.advert.search',
            compact(
                'showRegion',
                'adverts'
            )
        );
    }
}
