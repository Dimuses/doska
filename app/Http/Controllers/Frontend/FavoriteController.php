<?php

namespace App\Http\Controllers\Frontend;

use App\Models\common\Favorite;
use App\Http\Controllers\Controller;
use App\Http\Services\AdvertService;
use App\Http\Services\CategoryService;
use App\Http\Services\RegionService;
use Auth;

/**
 * Site controller
 *
 * @author  Dimus <iron@te.net.ua>
 */
class FavoriteController extends Controller
{
    /**
     * Region service instance
     *
     * @var RegionService
     */
    private $_regionService;
    /**
     * Advert service instance
     *
     * @var AdvertService
     */
    private $_advertService;

    /**
     * Category service instance
     *
     * @var CategoryService
     */
    private $_categoryService;
    /**
     * Favorite service instance
     *
     * @var \App\Http\Controllers\Frontend\FavoritesService
     */
    private $_favoritesService;

    /**
     * SiteController constructor.
     *
     * @param RegionService    $regionService    instance
     * @param AdvertService    $advertService    instance
     * @param CategoryService  $categoryService  instance
     * @param FavoritesService $favoritesService instance
     */
    public function __construct(
        RegionService $regionService,
        AdvertService $advertService,
        CategoryService $categoryService,
        FavoritesService $favoritesService
    ) {
        $this->_regionService = $regionService;
        $this->_advertService = $advertService;
        $this->_categoryService = $categoryService;
        $this->_favoritesService = $favoritesService;
        $this->middleware('auth');
    }

    /**
     * Index action
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        $userId = \Auth::id();
        $adverts = $this->_favoritesService->getAllByUser($userId);
        return view('frontend.favorites.index', compact('adverts'));
    }

    /**
     * Store favorite action
     *
     * @param FavoritesCreateRequest $request instance
     *
     * @return bool
     */
    public function store(FavoritesCreateRequest $request)
    {
        return (string)$this->_favoritesService
            ->create($request->get('advert_id', 0), Auth::id());
    }

    /**
     * Destroy action
     *
     * @param integer                  $favoriteId id
     * @param \Illuminate\Http\Request $request    instance
     *
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function destroy($favoriteId, \Illuminate\Http\Request $request)
    {
        try {
            $result = Favorite::where('advert_id', '=', $favoriteId)
                ->where('user_id', '=', Auth::id())
                ->delete();

        } catch (\Exception $e) {
            $result = false;
        }
        if ($request->ajax()) {
            return (string)$result;
        } else {
            return redirect()->route('frontend.favorites.index');
        }
    }
}
