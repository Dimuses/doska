<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdvertUserCreateRequest;
use App\Http\Requests\AdvertUserUpdateRequest;
use App\Http\Services\AdvertService;
use App\Http\Services\CategoryService;
use App\Http\Services\RegionService;
use App\Http\Services\UserService;
use App\Models\common\Advert;
use App\Models\common\AdvertPhoto;
use App\Models\common\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * AdvertController implements the CRUD actions for Advert model.
 *
 * @author Dimus <iron@te.net.ua>
 */
class AdvertUserController extends Controller
{
    /**
     * Region service instance
     *
     * @var RegionService
     */
    private $_regionService;

    /**
     * Category service instance
     *
     * @var CategoryService
     */
    private $_categoryService;

    /**
     * User service instance
     *
     * @var UserService
     */
    private $_userService;

    /**
     * Advert service instance
     *
     * @var AdvertService
     */
    private $_advertService;

    /**
     * AdvertController constructor.
     *
     * @param AdvertService   $advertService   instance
     * @param RegionService   $regionService   instance
     * @param UserService     $userService     instance
     * @param CategoryService $categoryService instance
     */
    public function __construct(
        AdvertService $advertService,
        RegionService $regionService,
        UserService $userService,
        CategoryService $categoryService
    ) {
        $this->_regionService = $regionService;
        $this->_categoryService = $categoryService;
        $this->_userService = $userService;
        $this->_advertService = $advertService;
        $this->middleware('auth');
    }

    /**
     * Index action
     *
     * @return string
     */
    public function index()
    {
        /**@var $user User */
        $user = \Auth::user();
        $opened = $this->_advertService->getOpened($user->id);
        $closed = $this->_advertService->getClosed($user->id);

        return view('frontend.advert-user.index', compact('opened', 'closed'));
    }

    /**
     * Create advert action
     *
     * @return array|Factory|View|mixed
     */
    public function create()
    {
        return view(
            'frontend.advert-user.create',
            [
                'regions'    => $this->_regionService->getAllActive(),
                'categories' => $this->_categoryService->getSubCategories(),
                'statuses'   => $this->_advertService->getUserStatuses(),
            ]
        );
    }

    /**
     * Save advert action
     *
     * @param AdvertUserCreateRequest $request instance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdvertUserCreateRequest $request)
    {
        $all = $request->all();
        $all['author_id'] = \Auth::id();

        try {
            $this->_advertService->saveWithPhoto(
                Advert::make(),
                $all,
                array_get($request, 'images', [])
            );
        } catch (\Throwable $e) {
            $request->session()->flash('advert', 'Error storing');
        }
        return redirect()->route('frontend.advert-user.index');
    }

    /**
     * Edit advert action
     *
     * @param Advert $advertUser instance
     *
     * @return array|Factory|View|mixed
     */
    public function edit(Advert $advertUser)
    {
        try {
            $this->authorize('update', $advertUser);
            return view(
                'frontend.advert-user.update',
                [
                    'regions'    => $this->_regionService->getAllActive(),
                    'categories' => $this->_categoryService->getSubCategories(),
                    'statuses'   => $this->_advertService->getAllStatuses(),
                    'advert'     => $advertUser,
                ]
            );
        } catch (AuthorizationException $e) {
            return abort(404);
        }
    }

    /**
     * Update advert action
     *
     * @param Advert                  $advertUser instance
     * @param AdvertUserUpdateRequest $request    instance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Advert $advertUser, AdvertUserUpdateRequest $request)
    {
        try {
            $this->authorize('update', $advertUser);
            $all = $request->all();
            try {
                $this->_advertService->saveWithPhoto(
                    $advertUser,
                    $all,
                    array_get($request, 'images', [])
                );
            } catch (\Throwable $e) {
                $request->session()->flash('advert', 'Error update');
            }
            return redirect()->route('frontend.advert-user.index');
        } catch (AuthorizationException $e) {
            return abort(404);
        }
    }

    /**
     * Delete advert action
     *
     * @param Advert  $advertUser instance
     * @param Request $request    instance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Advert $advertUser, Request $request)
    {
        try {
            $this->authorize('delete', $advertUser);
            try {
                $this->_advertService->deleteAdvert($advertUser);
                return redirect()->route('frontend.advert-user.index');
            } catch (\Throwable $e) {
                $request->session()->flash('advert', 'Error delete');
            }
            return redirect()->route('frontend.advert-user.index');
        } catch (AuthorizationException $e) {
            return abort(403, 'Unauthorized action');
        }
    }

    /**
     * Delete photo from advert action by ajax
     *
     * @param AdvertPhoto $photo   instance
     * @param Request     $request instance
     *
     * @return array
     */
    public function deletePhoto(AdvertPhoto $photo, Request $request)
    {
        if ($request->ajax()) {
            try {
                $this->authorize('deletePhoto', $photo);
                try {
                    $this->_advertService->deletePhoto($photo->title);
                    return [];
                } catch (\Throwable $e) {
                    return ['error' => "Error deleting"];
                }
            } catch (AuthorizationException $e) {
                return ['error' => "Unauthorized action"];
            }
        }
        return abort(403, 'Unauthorized action');
    }

    /*public function actionClose()
    {
    }*/
}
