<?php

namespace App\Http\Composers;

use App\Http\Services\CategoryService;
use App\Http\Services\RegionService;
use Illuminate\Contracts\View\View;

/**
 * Class FrontendLayoutComposer
 *
 * @package App\Http\Composers
 * @author  Dimus <iron@te.net.ua>
 */
class FrontendLayoutComposer
{

    /**
     * Region service class
     *
     * @var RegionService
     */
    private $_regionService;
    /**
     * Category service class
     *
     * @var CategoryService
     */
    private $_categoryService;

    /**
     * Construct
     * @param RegionService   $regionService   instance
     * @param CategoryService $categoryService instance
     */
    public function __construct(
        RegionService $regionService,
        CategoryService $categoryService
    ) {
        $this->_regionService = $regionService;
        $this->_categoryService = $categoryService;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view instance
     *
     * @return void
     */
    public function compose(View $view)
    {
        $regionId = session()->get('region', null);
        $view->with(
            [
                'regions' => $this->_regionService->getAllActive(),
                'selectedRegion' => $this->_regionService->get($regionId),
                'categories' => $this->_categoryService->getAll(),
            ]
        );
    }
}