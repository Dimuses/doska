<?php

namespace App\Http\Controllers\Frontend;

use App\Models\common\Advert;
use App\Models\common\Favorite;

/**
 * Class FavoritesService
 *
 * @package App\Http\Controllers\Frontend
 * @author  Dimus <iron@te.net.ua>
 */
class FavoritesService
{
    /**
     * Add favorite for user into database
     *
     * @param int $advertId id
     * @param int $userId   id
     *
     * @return bool
     */
    public function create($advertId, $userId)
    {
        try {
            Favorite::where(
                [
                    ['advert_id', '=', $advertId],
                    ['user_id', '=', $userId],
                ]
            )->delete();
        } catch (\Exception $e) {
        }

        if (Favorite::create(['advert_id' => $advertId, 'user_id' => $userId])) {
            return true;
        }
        return false;
    }

    /**
     * Determine if advert is favorite
     *
     * @param int  $advertId id
     * @param null $userId   id
     *
     * @return bool
     */
    public function isFavorite($advertId, $userId = null)
    {
        if (!$userId) {
            return false;
        }
        $count = Favorite::where(
            [
                ['advert_id', '=', $advertId],
                ['user_id', '=', $userId],
            ]
        )->count();
        return (bool)$count;
    }

    /**
     * Get all favorite adverts by user
     *
     * @param int $userId   id
     * @param int $paginate adverts per page
     *
     * @return mixed
     */
    public function getAllByUser($userId, $paginate = 15)
    {
        return Advert::sortable()
            ->with('favorites')
            ->where('status', '=', Advert::STATUS_ACTIVE)
            ->whereHas(
                'favorites',
                function ($query) use ($userId) {
                    return $query->where('user_id', '=', $userId);
                }
            )
            ->latest()
            ->paginate($paginate);
    }
}