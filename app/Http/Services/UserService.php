<?php

namespace App\Http\Services;

use App\Models\common\User;

/**
 * Class UserService
 *
 * @package App\Http\Services
 * @author  Dimus <iron@te.net.ua>
 */
class UserService
{
    /**
     * Get all user statuses
     *
     * @return array
     */
    public function getAllStatuses()
    {
        return User::getStatuses();
    }

    /**
     * Find user by string fitting his name
     *
     * @param string $query search query
     *
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function findByName($query)
    {
        return User::where('username', 'like', $query . '%')->get();
    }

    /**
     * Create user
     *
     * @param array $data data from request
     *
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    public function create($data)
    {
        return User::create(
            [
                'username'  => array_get($data, 'username'),
                'email'     => array_get($data, 'email'),
                'status'    => array_get($data, 'status'),
                'phone'     => array_get($data, 'phone'),
                'password'  => User::passwordHash(array_get($data, 'password')),
                'region_id' => array_get($data, 'region_id'),
            ]
        );
    }
}