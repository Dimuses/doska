<?php

namespace App\Http\Services;

use App\Models\common\Advert;
use App\Models\common\AdvertPhoto;
use App\Models\common\Category;
use App\Models\common\Region;
use DB;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Storage;

/**
 * Class UserService
 *
 * @package App\Http\Services
 * @author  Dimus <iron@te.net.ua>
 */
class AdvertService
{
    /**
     * Get all adverts
     *
     * @return Region[]|Collection
     */
    public function getAll()
    {
        return Advert::all();
    }

    /**
     * Get all advert statuses
     *
     * @return array
     */
    public function getAllStatuses()
    {
        return Advert::getStatuses();
    }

    /**
     * Get all advert user statuses
     *
     * @return array
     */
    public function getUserStatuses()
    {
        return Advert::getUserStatuses();
    }

    /**
     * Get all  advert types
     *
     * @return array
     */
    public function getAllTypes()
    {
        return Advert::getTypes();
    }

    /**
     * Delete photo from the advert
     *
     * @param string $name Photo advert name
     *
     * @return bool
     */
    public function deletePhoto($name)
    {
        $photo = $this->getByName($name);
        $paths = "{$photo->advert_id}/{$photo->title}";
        if (Storage::disk('adverts')->delete($paths)) {
            AdvertPhoto::destroy($photo->id);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete all photos from the advert
     *
     * @param Advert $advert advert instance
     *
     * @throws \Throwable
     * @return void
     */
    public function deleteAllPhotos(Advert $advert)
    {
        $folderExists = Storage::disk('adverts')->exists("{$advert->id}");
        $deleted = !Storage::disk('adverts')->deleteDirectory(("{$advert->id}"));

        if ($folderExists && $deleted) {
            throw  new \Exception();
        }
    }

    /**
     * Delete advert instance
     *
     * @param Advert $advert advert instance
     *
     * @throws \Throwable
     * @return void
     */
    public function deleteAdvert(Advert $advert)
    {
        DB::transaction(
            function () use ($advert) {
                $advert->delete();
                $this->deleteAllPhotos($advert);
            }
        );
    }

    /**
     * Get an advert photo by name
     *
     * @param string $name AdvertPhoto name
     *
     * @return AdvertPhoto|AdvertPhoto[]|Collection|Model
     */
    public function getByName($name)
    {
        return AdvertPhoto::whereTitle($name)->first();
    }

    /**
     * Save advert with photos
     *
     * @param Advert $advert     instance
     * @param array  $advertData data from request
     * @param array  $images     images array
     *
     * @throws \Throwable
     * @return void
     */
    public function saveWithPhoto(Advert $advert, $advertData, $images)
    {
        DB::transaction(
            function () use ($advert, $images, $advertData) {
                $advert->slug = null;
                $advert->fill($advertData)->save();
                if ($images && $advert->isDonateType()) {
                    foreach ($images as $image) {
                        /* @var  UploadedFile $image */
                        $image->storePublicly($advert->id, ['disk' => 'adverts']);
                        $aPhoto = new AdvertPhoto(['title' => $image->hashName()]);
                        $aPhoto->advert()->associate($advert);
                        $aPhoto->save();
                    }
                } elseif (!$advert->isDonateType()) {
                    $this->deleteAllPhotos($advert);
                    $this->deleteAllPhotosFromDb($advert);
                }
            }
        );
    }

    /**
     * Prepare gallery data
     *
     * @param Advert $advert advert instance
     *
     * @return array
     */
    public function prepareGalleryData(Advert $advert)
    {
        $initialPreview = [];
        $initialPreviewConfig = [];

        foreach ($advert->photos as $photo) {
            $initialPreview[] = $advert->getPhotoUrl($photo);
            $initialPreviewConfig[] = [
                'url' => route(
                    'admin.adverts.destroyPhoto',
                    ['id' => $photo->title]
                ),
            ];
        }
        return compact('initialPreview', 'initialPreviewConfig');
    }

    /**
     * Delete all photos from db
     *
     * @param Advert $advert instance
     *
     * @throws \Exception
     * @return void
     */
    private function deleteAllPhotosFromDb(Advert $advert)
    {
        AdvertPhoto::where(['advert_id' => $advert->id])->delete();
    }

    /**
     * Get all active adverts
     *
     * @param string $type     advert type (donate or receive)
     * @param null   $region   adverts from selected region
     * @param null   $category adverts from selected category
     * @param null   $sort     adverts from selected category
     * @param int    $paginate pages per the page
     *
     * @return Advert[]|Builder[]|Collection
     */
    public function getAllActive(
        $type,
        $region = null,
        $category = null,
        $sort = null,
        $paginate = 16
    ) {
        $categories = [];
        if ($category) {
            $categories = Category::where('parent_id', '=', $category)->pluck('id');
            $categories[] = $category;
        }

        $query = Advert::sortable()
            ->with(
                [
                    'category',
                    'category.parent',
                    'photos',
                    'author',
                    'region',
                    'favorites' => function ($hasMany) {
                        $hasMany->where('user_id', \Auth::id());
                    },
                ]
            )
            ->where(
                [
                    ['status', '=', Advert::STATUS_ACTIVE],
                    ['type', '=', $type]
                ]
            )
            ->whereHas(
                'region',
                function (Builder $query) {
                    return $query->where('status', '=', Region::STATUS_ACTIVE);
                }
            );
        if ($region) {
            $query->where('region_id', '=', $region);
        }
        if ($categories) {
            $query->whereIn('category_id', $categories);
        }
        $sort && $sort == 'asc'
            ? $query->first()
            : $query->latest();
        return $query->paginate($paginate);
    }

    /**
     * Update advert views
     *
     * @param int $advertId id
     * @param int $int      count
     *
     * @return int
     */
    public function updateViews($advertId, $int = 1)
    {
        return Advert::where('id', '=', $advertId)
            ->increment('views', $int);
    }

    /**
     * Get 5 adverts randomly
     *
     * @param Advert $advert
     *
     * @return Advert[]|Builder[]|Collection
     */
    public function getRelated(Advert $advert)
    {
        $count = Advert::where('category_id', '=', $advert->category_id)
            ->where('status', '=', Advert::STATUS_ACTIVE)
            ->where('id', '!=', $advert->id)
            ->where('type', '=', Advert::TYPE_DONATE)
            ->count();

        $rand = rand(0, $count - 5);

        return Advert::where('category_id', '=', $advert->category_id)
            ->where('status', '=', Advert::STATUS_ACTIVE)
            ->where('id', '!=', $advert->id)
            ->where('type', '=', Advert::TYPE_DONATE)
            ->limit(4)
            ->offset($rand)
            ->get();
    }

    /**
     * Search for frontend
     *
     * @param array $data
     *
     * @return Advert[]|LengthAwarePaginator|Collection
     */
    public function search($data)
    {
        $adverts = Advert::search(
            [
                'title'       => array_get($data, 'query'),
                'region_id'   => array_get($data, 'region_id'),
                'category_id' => array_get($data, 'category_id'),
                'status'      => Advert::STATUS_ACTIVE,
            ],
            true,
            15
        );
        return $adverts;
    }

    /**
     * Get all closed adverts
     *
     * @param int $advertId
     *
     * @return Advert[]|Collection
     */
    public function getClosed($advertId)
    {
        return Advert::where(
            [
                [
                    'status',
                    '=',
                    Advert::STATUS_INACTIVE,
                ],
                [
                    'author_id',
                    '=',
                    $advertId,
                ],
                [
                    'status',
                    '!=',
                    Advert::STATUS_BLOCKED,
                ],
            ]
        )
            ->orderBy('created_at', SORT_DESC)
            ->get();
    }

    /**
     * Get all opened adverts
     *
     * @param int $advertId
     *
     * @return Advert[]|Collection
     */
    public function getOpened($advertId)
    {
        return Advert::where(
            [
                [
                    'status',
                    '=',
                    Advert::STATUS_ACTIVE,
                ],
                [
                    'author_id',
                    '=',
                    $advertId,
                ],
                [
                    'status',
                    '!=',
                    Advert::STATUS_BLOCKED,
                ],
            ]
        )
            ->orderBy('created_at', SORT_DESC)
            ->get();
    }
}