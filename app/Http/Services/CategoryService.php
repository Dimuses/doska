<?php

namespace App\Http\Services;

use App\Models\common\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UserService
 *
 * @package App\Http\Services
 * @author  Dimus <iron@te.net.ua>
 */
class CategoryService
{
    /**
     * Get all categories
     *
     * @return Category[]|Collection
     */
    public function getAll()
    {
        return Category::all();
    }

    /**
     * Get all categories without id
     *
     * @param int $categoryId id
     *
     * @return Category[]|Builder[]|Collection
     */
    public function getAllWithout($categoryId)
    {
        return Category::with('parent')
            ->whereRaw('id !=' . $categoryId)
            ->get();
    }

    /**
     * Get all subcat
     *
     * @param int|null $exclude id
     *
     * @return Category[]|Builder[]|Collection
     */
    public function getSubCategories(int $exclude = null)
    {
        $query = Category::with('parent')
            ->whereNotNull('parent_id');
        if ($exclude != null) {
            $query->where('id', '!=', $exclude);
        }
        return $query->get();
    }

    /**
     * Get all top categories
     *
     * @param int|null $exclude id
     *
     * @return Category[]|Builder[]|Collection
     */
    public function getTopCategories(int $exclude = null)
    {
        $query = Category::with('parent')
            ->whereNull('parent_id');
        if ($exclude != null) {
            $query->where('id', '!=', $exclude);
        }
        return $query->get();
    }

    /**
     * Get category by slug
     *
     * @param string $slug slug
     *
     * @return Category[]|Collection
     */
    public function getBySlug($slug)
    {
        return Category::where('slug', '=', $slug)->get();
    }
}