<?php

namespace App\Http\Services;

use App\Models\common\Region;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UserService
 * @package App\Http\Services
 * @author  Dimus <iron@te.net.ua>
 */
class RegionService
{
    /**
     * UserService constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get all regions
     *
     * @return Region[]|Collection
     */
    public function getAll()
    {
        return Region::all();
    }

    /**
     * Get all region statuses
     *
     * @return array
     */
    public function getAllStatuses()
    {
        return Region::getStatuses();
    }

    /**
     * Get one region by id
     *
     * @param int $regionId id
     *
     * @return Region|Region[]|Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function get($regionId)
    {
        return Region::find($regionId);
    }

    /**
     * Get all active regions
     *
     * @return Region[]|Collection
     */
    public function getAllActive()
    {
        return Region::where(['status' => Region::STATUS_ACTIVE])->get();
    }
}