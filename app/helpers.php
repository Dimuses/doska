<?php

if (!function_exists('floatToString')) {
    /**
     * Convert float to string
     *
     * @param float $number number
     *
     * @return mixed
     */
    function floatToString($number)
    {
        // . and , are the only decimal separators known in ICU data,
        // so its safe to call str_replace here
        return str_replace(',', '.', (string)$number);
    }
}

if (!function_exists('array_index')) {
    /**
     * Index array by key
     *
     * @param array  $array
     * @param string $key
     * @param array  $groups
     *
     * @return array
     */
    function array_index($array, $key, $groups = [])
    {
        $result = [];
        $groups = (array)$groups;

        foreach ($array as $element) {
            $lastArray = &$result;

            foreach ($groups as $group) {
                $value = array_get($element, $group);
                if (!array_key_exists($value, $lastArray)) {
                    $lastArray[$value] = [];
                }
                $lastArray = &$lastArray[$value];
            }

            if ($key === null) {
                if (!empty($groups)) {
                    $lastArray[] = $element;
                }
            } else {
                $value = array_get($element, $key);
                if ($value !== null) {
                    if (is_float($value)) {
                        $value = floatToString($value);
                    }
                    $lastArray[$value] = $element;
                }
            }
            unset($lastArray);
        }

        return $result;
    }
}
if (!function_exists('buildSortString')) {
    /**
     * Function build a sort url for adverts
     *
     * @param string $sortParameter   by default created_at
     * @param string $direction       asc or desc
     * @param array  $queryParameters default
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    function buildSortString($sortParameter, $direction, $queryParameters = [])
    {
        $checkStrlenOrArray = function ($element) {
            return is_array($element)
                ? $element
                : strlen($element);
        };

        $persistParameters = array_filter(
            request()->except(
                'sort',
                'direction',
                'page'
            ),
            $checkStrlenOrArray
        );
        $queryString = http_build_query(
            array_merge(
                $queryParameters,
                $persistParameters,
                [
                    'sort' => $sortParameter,
                    'direction' => $direction,
                ]
            )
        );

        return url(request()->path() . '?' . $queryString);
    }
}