<?php

namespace App\Models\frontend;

/**
 * Class Advert
 *
 * @package App\Models\frontend
 * @author  Dimus <iron@te.net.ua>
 */
class Advert extends \App\Models\common\Advert
{
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

}
