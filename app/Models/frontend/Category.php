<?php

namespace App\Models\frontend;

/**
 * Class Category
 *
 * @package App\Models\frontend
 * @author  Dimus <iron@te.net.ua>
 */
class Category extends \App\Models\common\Category
{
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
