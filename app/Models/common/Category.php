<?php

namespace App\Models\common;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

/**
 * App\Models\common\Category
 *
 * @property int                             $id
 * @property string                          $title
 * @property string                          $slug
 * @property string|null                     $seo_title
 * @property string|null                     $seo_description
 * @property string|null                     $seo_text
 * @property int|null                        $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @property-read Collection|Category[]      $children
 * @property-read Category|null              $parent
 * @mixin         \Eloquent
 *
 * @author Dimus <iron@te.net.ua>
 */
class Category extends Model
{
    use Sluggable;
    use Sortable;

    /**
     * Fillable attr
     *
     * @var array
     */
    protected $fillable = [
        'title', 'parent_id', 'seo_title', 'seo_description', 'seo_text',
    ];
    /**
     * Sortable attr
     *
     * @var array
     */
    public $sortable = ['id', 'title', 'parent_id'];

    /**
     * Search category
     *
     * @param array $params   search params
     * @param bool  $sortable flag
     * @param null  $paginate categories per page
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public static function search($params, $sortable = true, $paginate = null)
    {
        $model = new self();

        if ($sortable) {
            $query = self::sortable()->with('parent');
        } else {
            $query = $model->newQuery();
        }
        if ($value = array_get($params, 'id')) {
            $query->where('id', $value);
        }
        if ($value = array_get($params, 'title')) {
            $query->where('title', $value);
        }
        if ($value = array_get($params, 'parent_id')) {
            $query->where('parent_id', $value);
        }
        if ($paginate) {
            return $query->paginate($paginate);
        } else {
            return $query->get();
        }
    }

    /**
     * Scope
     *
     * @param Builder $query instance
     *
     * @return mixed
     */
    public function scopeTopLevel(Builder $query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    /**
     * Get related parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * Get related children
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }
}
