<?php

namespace App\Models\common;

use Actuallymab\LaravelComment\Commentable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class
 *
 * @package App
 * @mixin   \Eloquent
 * @author  Dimus <iron@te.net.ua>
 */
class Advert extends Model
{
    use Notifiable;
    use Sortable;
    use Sluggable;
    use Commentable;

    /**
     * Sortable properties
     *
     * @var array
     */
    public $sortable = [
        'id',
        'title',
        'category_id',
        'region_id',
        'views',
        'created_at',
    ];
    /**
     * Guarded properties
     *
     * @var array
     */
    protected $guarded = ['images'];

    /**
     * Active status name
     */
    const STATUS_ACTIVE = 'active';
    /**
     * InActive status name
     */
    const STATUS_INACTIVE = 'inactive';
    /**
     * Blocked status name
     */
    const STATUS_BLOCKED = 'blocked';
    /**
     * Type donate name
     */
    const TYPE_DONATE = 'donate';
    /**
     * Type receive name
     */
    const TYPE_RECEIVE = 'receive';

    /**
     * Get all adverts statuses
     *
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_INACTIVE,
            self::STATUS_BLOCKED,
        ];
    }

    /**
     * Get all adverts user statuses
     *
     * @return array
     */
    public static function getUserStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_INACTIVE,
        ];
    }

    /**
     * Search an advert by parameters
     *
     * @param array $params   search data
     * @param bool  $sortable flag
     * @param null  $paginate adverts per page
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public static function search(
        $params,
        $sortable = true,
        $paginate = null
    ) {
        $model = new self();

        if ($sortable) {
            $query = self::sortable();
        } else {
            $query = $model->newQuery();
        }
        if ($value = array_get($params, 'id')) {
            $query->where('id', $value);
        }
        if ($value = array_get($params, 'title')) {
            $query->where('title', 'like', $value . '%');
        }
        if ($value = array_get($params, 'category_id')) {
            $query->where('category_id', $value);
        }
        if ($value = array_get($params, 'region_id')) {
            $query->where('region_id', $value);
        }
        if ($value = array_get($params, 'views')) {
            $query->where('views', 'like', $value . '%');
        }
        if ($value = array_get($params, 'status')) {
            $query->where('status', '=', $value);
        }
        $query->latest();

        if ($paginate) {
            return $query->paginate($paginate);
        } else {
            return $query->get();
        }
    }

    /**
     * Get advert types
     *
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_DONATE,
            self::TYPE_RECEIVE,
        ];
    }

    /**
     * Return sluggable attr
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    /**
     * Get related category
     *
     * @return BelongsTo|HasOne
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get related favorites
     *
     * @return HasMany
     */
    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    /**
     * Get related region
     *
     * @return BelongsTo|HasOne
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /**
     * Get related photos
     *
     * @return HasMany
     */
    public function photos()
    {
        return $this->hasMany(AdvertPhoto::class);
    }

    /**
     * Get related author
     *
     * @return BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if advert is active
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * Check if advert is inactive
     *
     * @return bool
     */
    public function isInActive()
    {
        return $this->status == self::STATUS_INACTIVE;
    }

    /**
     * Check if advert is blocked
     *
     * @return bool
     */
    public function isBlocked()
    {
        return $this->status == self::STATUS_BLOCKED;
    }

    /**
     * Check if advert is donate type
     *
     * @return bool
     */
    public function isDonateType()
    {
        return $this->type == self::TYPE_DONATE;
    }

    /**
     *  Get advert photo url
     *
     * @param AdvertPhoto $photo instance
     *
     * @return string
     */
    public function getPhotoUrl(AdvertPhoto $photo)
    {
        return Storage::disk('adverts')->url($this->id . '/' . $photo->title);
    }

}
