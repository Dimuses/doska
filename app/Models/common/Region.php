<?php

namespace App\Models\common;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class Region
 *
 * @package App
 *
 * @property int                          $id
 * @property mixed                        status
 * @property string                       $title
 * @property string                       $slug
 * @property Carbon|null                  $created_at
 * @property Carbon|null                  $updated_at
 *
 * @property-read \App\Models\common\User $user
 *
 * @mixin \Eloquent
 *
 * @author Dimus <iron@te.net.ua>
 */
class Region extends Model
{
    use Sluggable;
    use Sortable;

    /**
     * Region status active
     */
    const STATUS_ACTIVE = 'active';
    /**
     * Region status inactive
     */
    const STATUS_INACTIVE = 'inactive';

    /**
     * Fillable attr
     *
     * @var array
     */
    protected $fillable = ['title', 'status'];
    /**
     * Sortable attr
     *
     * @var array
     */
    public $sortable = ['id', 'title'];

    /**
     * Get all region statuses
     *
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_INACTIVE,
        ];
    }

    /**
     * Search region by params
     *
     * @param array $params   params for searching
     * @param bool  $sortable flag
     * @param null  $paginate flag
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public static function search($params, $sortable = true, $paginate = null)
    {
        $model = new self();

        if ($sortable) {
            $query = self::sortable();
        } else {
            $query = $model->newQuery();
        }
        if ($value = array_get($params, 'id')) {
            $query->where('id', $value);
        }
        if ($value = array_get($params, 'title')) {
            $query->where('title', 'like', $value . '%');
        }
        if ($value = array_get($params, 'status')) {
            $query->where('status', $value);
        }
        if ($paginate) {
            return $query->paginate($paginate);
        } else {
            return $query->get();
        }
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    /**
     * Get related user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

    /**
     * Determine if region is active
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }
}
