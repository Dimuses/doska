<?php

namespace App\Models\common;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\common\Complaint
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\common\Complaint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\common\Complaint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\common\Complaint whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Complaint extends Model
{
    //
}
