<?php

namespace App\Models\common;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;

/**
 * Class User
 *
 * @package  App
 * @property mixed                               status
 * @property mixed                               username
 * @property mixed                               region_id
 * @property mixed                               email
 * @property mixed                               phone
 * @property int                                 $id
 * @property string                              $password
 * @property string|null                         $remember_token
 * @property \Illuminate\Support\Carbon|null     $created_at
 * @property \Illuminate\Support\Carbon|null     $updated_at
 * @property mixed                               is_admin
 *
 * @property-read DatabaseNotificationCollection $notifications
 * @property-read \App\Models\common\Region      $region
 *
 * @mixin \Eloquent
 *
 * @author Dimus <iron@te.net.ua>
 */
class  User extends Authenticatable
{
    use Notifiable;
    use Sortable;

    /**
     * Sortable attr
     *
     * @var array
     */
    public $sortable = ['id', 'username', 'email', 'status'];

    /**
     * Value of active user status
     */
    const STATUS_ACTIVE = 'active';
    /**
     * Value of inactive user status
     */
    const STATUS_INACTIVE = 'inactive';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'region_id', 'status', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all user statuses
     *
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE,
            self::STATUS_INACTIVE,
        ];
    }

    /**
     * Get related region
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /**
     * Determine if user is active
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * Search user by params
     *
     * @param array $params   params for searching
     * @param bool  $sortable flag
     * @param null  $paginate flag
     *
     * @return LengthAwarePaginator|Builder[]|Collection
     */
    public static function search($params, $sortable = true, $paginate = null)
    {
        $model = new self();

        if ($sortable) {
            $query = self::sortable()->with('region');
        } else {
            $query = $model->newQuery()->with('region');
        }
        if ($value = array_get($params, 'id')) {
            $query->where('id', $value);
        }
        if ($value = array_get($params, 'username')) {
            $query->where('username', 'like', $value . '%');
        }
        if ($value = array_get($params, 'email')) {
            $query->where('email', 'like', $value . '%');
        }
        if ($value = array_get($params, 'status')) {
            $query->where('status', $value);
        }
        if ($value = array_get($params, 'region_id')) {
            $query->where('region_id', $value);
        }
        if ($paginate) {
            return $query->paginate($paginate);
        } else {
            return $query->get();
        }
    }

    /**
     * Method for hashing password
     *
     * @param string $password password
     *
     * @return string
     */
    public static function passwordHash($password)
    {
        return bcrypt($password);
    }

    /**
     * Determine if user is admin
     *
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->is_admin;
    }
}
