<?php

namespace App\Models\common;

/**
 * App\Models\common\AdvertPhoto
 *
 * @property int                             $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int                             $advert_id
 * @property string                          $title
 *
 * @property-read \App\Models\common\Advert  $advert
 * @mixin         \Eloquent
 */
/**
 * Class AdvertPhoto
 *
 * @package App\Models\common
 * @author  Dimus <iron@te.net.ua>
 */
class AdvertPhoto extends \Eloquent
{
    /**
     * Fillable attr
     *
     * @var array
     */
    protected $fillable = ['title', 'advert_id'];

    /**
     * Get related advert
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }
}
