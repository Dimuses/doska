<?php

namespace App\Models\common;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\common\Favorite
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\common\Favorite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\common\Favorite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\common\Favorite whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Favorite extends Model
{
    /**
     * Fillable attr
     *
     * @var array
     */
    protected $fillable = ['advert_id', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }
}
