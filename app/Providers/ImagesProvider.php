<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class ImagesProvider
 *
 * @package App\Providers
 * @author  Dimus <iron@te.net.ua>
 */
class ImagesProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $finder = new \Symfony\Component\Finder\Finder();
        $isDirectory = \File::isDirectory(public_path('storage/images/adverts/'));

        if ($isDirectory) {
            @$finder->directories()->in(public_path('storage/images/adverts/'));
            $dirs = [];
            foreach ($finder as $i) {
                $dirs[] = $i->getPathname();
            }
            config(['imagecache.paths' => $dirs]);
        }

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
