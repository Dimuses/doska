<?php

namespace App\Providers;

use App\Models\common\Advert;
use App\Models\common\AdvertPhoto;
use App\Policies\AdminPolicy;
use App\Policies\AdvertPhotoPolicy;
use App\Policies\AdvertPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Advert::class => AdvertPolicy::class,
        AdvertPhoto::class => AdvertPhotoPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
