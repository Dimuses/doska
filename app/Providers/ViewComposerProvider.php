<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

/**
 * Class ViewComposerProvider
 *
 * @package App\Providers
 * @author  Dimus <iron@te.net.ua>
 */
class ViewComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            'frontend.layouts.main',
            'App\Http\Composers\FrontendLayoutComposer'
        );
        View::composer(
            'frontend.layouts.inner',
            'App\Http\Composers\FrontendLayoutComposer'
        );
        View::composer(
            'frontend.layouts.create_update',
            'App\Http\Composers\FrontendLayoutComposer'
        );
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
