<?php

namespace App\Filters;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

/**
 * Class Custom
 *
 * @package App\Filters
 * @author  Dimus <iron@te.net.ua>
 *
 */
class Custom implements FilterInterface
{
    /**
     * Custom filter for image ext
     *
     * @param Image $image image instance
     *
     * @return Image
     */
    public function applyFilter(Image $image)
    {
        return $image->fit(
            config('params.adverts.images.width'),
            config('params.adverts.images.height')
        );
    }

}