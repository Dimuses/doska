<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class Feedback
 *
 * @package App\Mail
 * @author  Dimus <iron@te.net.ua>
 */
class Feedback extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * User name from form
     *
     * @var string $name
     */
    public $name;
    /**
     * User email from form
     *
     * @var string $email
     */
    public $email;
    /**
     * User text from form
     *
     * @var string $text
     */
    public $text;

    /**
     * Feedback constructor.
     *
     * @param string $name  username
     * @param string $email email
     * @param string $text  text
     */
    public function __construct($name, $email, $text)
    {
        $this->name = $name;
        $this->email = $email;
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
            ->subject('Запрос в поддержку')
            ->view('frontend.mails.feedback');
    }
}
