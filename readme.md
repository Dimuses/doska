# Установка

Запустить скрипт start.sh из корня проекта. Деплой на Docker
В миграциях уже умеются демоданные

### Админ панель: 
- Uri:    **/admin**
- Доступ: **john@example.com / secret**

### Личный кабинет пользователя:
- Uri:    **/advert-user**
- Доступ: **nyah64@example.org / secret**