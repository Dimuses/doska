<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/', function () {
    return view('welcome');
}
);

Route::group(
    [
        'namespace' => 'Admin',
        'prefix'    => 'admin',
        'as'        => 'admin.',

    ], function () {
        Auth::routes();
        Route::group(
            [
                'middleware' => [
                    'auth:admin',
                ],
            ], function () {
            Route::get('/', 'AdvertController@index');
            Route::any('users/find', 'UserController@find')->name('users.find');
            Route::post('adverts/destroy-photo/', 'AdvertController@destroyPhoto')->name('adverts.destroyPhoto');
            Route::resource('adverts', 'AdvertController');
            Route::resource('users', 'UserController');
            Route::resource('regions', 'RegionController');
            Route::resource('categories', 'CategoryController');
        }
    );
}
);

Route::group(
    [
        'namespace' => 'Frontend',
        //'prefix' => 'frontend',
        'as'        => 'frontend.'
        //'middleware' => 'auth'
    ], function () {
    Auth::routes();
    Route::get('/', 'SiteController@index')->name('index');
    Route::get('/feedback', 'SiteController@feedback')->name('feedback');
    Route::post('/feedback', 'SiteController@sendFeedback')->name('send-feedback');
    Route::post('region/session', 'SiteController@session')->name('session');
    Route::get('search', 'AdvertController@search')->name('adverts.search');
    Route::get('{type}/{category?}', 'AdvertController@index')
        ->where(
            [
                'type' => 'donate|receive',
                'category' => '(\w|-)+',
            ]
        )
        ->name('adverts.index');
    Route::resource('adverts', 'AdvertController')->only(
        [
            'show',
        ]
    );
    Route::resource('advert-user', 'AdvertUserController');
    Route::delete('advert-user/delete-photo/{photo}', 'AdvertUserController@deletePhoto')->name(
        'advert-user.delete-photo'
    );
    Route::resource('favorites', 'FavoriteController');
}
);
