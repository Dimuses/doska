<?php
// Home
try {
//Frontend
    Breadcrumbs::for('frontend.adverts.index', function ($trail, $type, $category = null) {
        $types = ['donate' => 'Дарю', 'receive' => 'Приму',];
        if($category == null){
            $trail->push(__(ucfirst(array_get($types, $type))), route('frontend.adverts.index', ['type' => $type]));
        }elseif ($parent = $category->parent){
            $trail->parent('frontend.adverts.index', $type, $parent);
            $trail->push($category->title, route('frontend.adverts.index', ['type' => $type, 'category' => $category->slug]));
        }else{
            $trail->parent('frontend.adverts.index', $type, null);
            $trail->push($category->title, route('frontend.adverts.index', ['type' => $type, 'category' => $category->slug]));
        }
    });
    Breadcrumbs::for('frontend.adverts.show', function ($trail, $advert) {
        $trail->parent('frontend.adverts.index', $advert->type, $advert->category);
        $trail->push($advert->title, route('frontend.adverts.show', $advert));
    });

} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {}