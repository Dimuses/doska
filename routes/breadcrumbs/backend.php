<?php
// Home
try {
// Home > Blog
    Breadcrumbs::for('admin.users.index', function ($trail) {
        $trail->parent('home');
        $trail->push(__('Users'), route('admin.users.index'));
    });

    Breadcrumbs::for('admin.users.create', function ($trail) {
        $trail->parent('admin.users.index');
        $trail->push(__('Create new user'), route('admin.users.create'));
    });

    Breadcrumbs::for('admin.users.edit', function ($trail) {
        $trail->parent('admin.users.index');
        $trail->push(__('Update user'), route('admin.users.index'));
    });

    Breadcrumbs::for('admin.regions.index', function ($trail) {
        $trail->parent('home');
        $trail->push(__('Regions'), route('admin.regions.index'));
    });

    Breadcrumbs::for('admin.regions.create', function ($trail) {
        $trail->parent('admin.regions.index');
        $trail->push(__('Create new region'), route('admin.regions.create'));
    });

    Breadcrumbs::for('admin.regions.edit', function ($trail) {
        $trail->parent('admin.regions.index');
        $trail->push(__('Update region'), route('admin.regions.index'));
    });

    Breadcrumbs::for('admin.categories.index', function ($trail) {
        $trail->parent('home');
        $trail->push(__('Categories'), route('admin.categories.index'));
    });
    Breadcrumbs::for('admin.categories.create', function ($trail) {
        $trail->parent('admin.categories.index');
        $trail->push(__('Create new category'), route('admin.categories.create'));
    });
    Breadcrumbs::for('admin.categories.edit', function ($trail) {
        $trail->parent('admin.categories.index');
        $trail->push(__('Update category'), route('admin.categories.index'));
    });
    Breadcrumbs::for('admin.adverts.index', function ($trail) {
        $trail->parent('home');
        $trail->push(__('Adverts'), route('admin.adverts.index'));
    });
    Breadcrumbs::for('admin.adverts.create', function ($trail) {
        $trail->parent('admin.adverts.index');
        $trail->push(__('Create new adverts'), route('admin.adverts.create'));
    });
    Breadcrumbs::for('admin.adverts.edit', function ($trail) {
        $trail->parent('admin.adverts.index');
        $trail->push(__('Update advert'), route('admin.adverts.index'));
    });
} catch (\DaveJamesMiller\Breadcrumbs\Exceptions\DuplicateBreadcrumbException $e) {}