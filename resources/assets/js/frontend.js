$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

$(document).ready(function () {
    if ($().ikSelect) {
        $('.js-select').ikSelect({autoWidth: false, ddFullWidth: false});
        $('.js-select-search').ikSelect({autoWidth: false, ddFullWidth: false, customClass: 'search-select'});
    }
});
$(function () {
    $(document.body).on('click touch', '.js-label-check', function (e) {
        e.preventDefault();
        var
            $this = $(this), $input = $this.find('input'), unchecked = $this.hasClass('label-uncheck');
        if ($input.length && $this.hasClass('active') && unchecked) {
            $this.removeClass('active');
            $input.prop('checked', false);
        } else if ($input.length && !$this.hasClass('active')) {
            $('.js-label-check.active').removeClass('active').find('input').prop('checked', false);
            $this.addClass('active');
            $input.prop('checked', true);
        }
    })
});
$(function () {
    function getFiles() {
        return $('.photo_loader_item:not(.photo_loader_item_template, .photo_loader_item_add)');
    }

    function checkFilesLimit() {
        var $addItem = $('.photo_loader_item_add');
        var $files = getFiles();
        if ($files.length >= 8) {
            $addItem.hide();
            return false;
        }
        $addItem.show();
        return true;
    }

    function setImageDataUrl(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var $files = getFiles();
                if ($files.length < 9) {
                    showPhotoToUpload(input, e.target.result);
                    initEvents();
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function showPhotoToUpload(input, dataUrl) {
        var $button = $('[data-upload-photo]');
        var $container = $button.parent();
        var formName = $container.parents('form').data('name');
        var $template = $('.photo_loader_item_template');
        var $item = $template.clone();
        var $img = $item.find('.photo_loader_img').css('background-image', 'url(' + dataUrl + ')');
        var $input = $(input);
        $input.hide();
        $input.attr('name', "images[]");
        $item.append($input, $img);
        $item.removeClass('photo_loader_item_template');
        $container.before($item);
        checkFilesLimit();
    }

    var $photoLoader = $('#photo_loader');
    $photoLoader.find('.photo_loader_photos > input[type=file]').each(function (i, input) {
        $(input).hide();
    });
    $('[data-upload-photo]').click(function () {
        if (!checkFilesLimit()) {
            return;
        }
        var $file = $('<input type="file" accept="image/jpeg,image/png" style="display: none;">');
        $('.photo_loader_photos').before($file);
        $file.get(0).addEventListener("change", function () {
            if (!$(this).val()) {
                $file.remove();
                return;
            }
            setImageDataUrl(this);
        });
        $file.click();
    });

    function initEvents() {
        $('.photo_loader_del').on('click', function (e) {
            var el = $(this);
            var attr = el.attr('data-delete-photo');
            if (attr.length > 0) {
                $.post(attr, {_method: 'delete'}, function (resp) {
                    if (!('error' in resp)) {
                        el.parent('.photo_loader_item').remove();
                        checkFilesLimit();
                    }
                }, 'json');
            } else {
                el.parent('.photo_loader_item').remove();
                checkFilesLimit();
            }
        });
    }

    initEvents();
});
$(function () {
    var $active = $('.js-change-ads-type.active'), $imagesBlock = $('.upload-previews');
    if ($active.length) {
        toggleUploader($active.find('[data-uploader]').data('uploader'));
    }
    $(document.body).on('click', '.js-change-ads-type label', function () {
        var
            $this = $(this);
        $('.js-change-ads-type.active').removeClass('active');
        $this.parents('.js-change-ads-type').addClass('active');
        toggleUploader($this.data('uploader'));
    });
    function toggleUploader(action) {
        if (action === 'show') {
            $imagesBlock.show()
        } else if (action === 'hide') {
            $imagesBlock.hide()
        }
    }
});
