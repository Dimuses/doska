@php
    $file = array_get($model, 'photos.0');
    $url = $file ? url("/imagecache/custom/{$file->title}") : url('/img/dummy.png');
@endphp

<div class="col-sm-4 col-md-3">
    <div class="product-item">
        <a class="product-item__img" href="">
            <img src="{{ $url }}">
        </a>
        <div class="product-item__desc">
            <a class="product-item__title"
               href="">{{$model->title }}</a>
            <div class="clearfix">
                <span class="product-item__date fleft">
                      {{ $model->created_at->formatLocalized('%d %b %Y') }}
                </span>
                <span class="product-metadata__views fright">
                    {{(int)$model->views }}
                </span>
            </div>
            <form action="{{route('frontend.favorites.destroy', $model)}}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-default" onclick="confirm('Вы уверены')">Удалить из избранного</button>
            </form>
        </div>
    </div>
</div>
