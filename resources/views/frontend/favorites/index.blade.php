@extends('frontend.layouts.inner')
@php $title = 'Избранное'; @endphp
@section('title', $title)
@section('content')
    <h1 class="title">{{$title}}</h1>
    @if ($adverts->count())
        @include('frontend.blocks.sort', ['urlDesc' => buildSortString('created_at', 'desc'), 'urlAsc' => buildSortString('created_at', 'asc')])
        <div class="row catalog-list">
            @each('frontend.favorites._list', $adverts, 'model')
        </div>
        {!! $adverts->appends(Request::except('page'))->render() !!}
    @else
        <p>Ничего не найдено</p>
    @endif
@endsection