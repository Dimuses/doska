{{$slot}}

@push('scripts')
    <script>
        $(function () {
            var loginForm = $("#{{$formId}}");

            loginForm.submit(function (e) { console.log(loginForm);
                e.preventDefault();
                $(".text-danger strong").text('');
                $(".has-error").removeClass('has-error');

                var formData = loginForm.serializeArray();
                $.ajax({
                    url: loginForm.attr('action'),
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        if(data.intended !== undefined){
                            window.location = data.intended;
                        }else {
                            window.location = '/';
                        }
                        console.log(data);
                    },
                    error: function (data) {
                        $.each(data.responseJSON.errors, function (i, v) {
                            $('[name="'+ i +'"]')
                            .next('.text-danger')
                            .find('strong')
                            .text(v[0])
                            .parents('.form-group')
                            .addClass('has-error');
                        });
                    }
                });
            });
        })
    </script>
@endpush