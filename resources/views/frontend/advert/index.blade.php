@extends('frontend.layouts.inner')
@section('title', $seo['title'])
@section('breadcrumbs', Breadcrumbs::render())
@section('content')
    @if ($adverts->count())
        @include('frontend.blocks.categories', [
          'adverts' => $adverts,
          'categoriesCollection' => $categoriesCollection,
          'region' => $region,
          'type' => $type,
      ])
        @include('frontend.blocks.sort', ['urlDesc' => buildSortString('created_at', 'desc'), 'urlAsc' => buildSortString('created_at', 'asc')])
        <div class="row catalog-list">
            @each("frontend.blocks.lists._$type", $adverts, 'model')
        </div>
        {!! $adverts->appends(Request::except('page'))->render() !!}

        @include('frontend.blocks.modals.favorites', [
            'btnClass' => 'product-item__favorite',
            'isDetail' => 'false'
        ])
    @else
        <p>Ничего не найдено</p>
    @endif
    <div>
        {{nl2br($seo['text']) }}
    </div>
@stop
<br>
