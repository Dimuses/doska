@extends('frontend.layouts.inner')
@section('title', 'Поиск')
@section('content')
    @if ($adverts->count())
        <h1 class="title title_bordered">{{trans_choice('app.search', $adverts->count(), ['value' => $adverts->count()])}} по запросу "{{request('q')}}"</h1>
        @include('frontend.blocks.sort', ['urlDesc' => buildSortString('created_at', 'desc'), 'urlAsc' => buildSortString('created_at', 'asc')])
        @each('frontend.blocks.lists._donate', $adverts, 'model')

        {!! $adverts->appends(Request::except('page'))->render() !!}

        @include('frontend.blocks.modals.favorites', [
            'btnClass' => 'product-item__favorite',
            'isDetail' => 'false'
        ])
    @else
        <p>Ничего не найдено</p>
    @endif
@stop

