@extends('frontend.layouts.inner')
@section('title', $advert->title)
@section('breadcrumbs', Breadcrumbs::render())

@section('content')
    <h1 class="title title_large">{{$advert->title}}</h1>
    <div class="product-metadata">
        <span class="product-metadata__info">размещено {{ $advert->created_at->formatLocalized('%d %b %Y') }} </span>
        <span class="product-metadata__views product-metadata__views_m-left">{{ (int)$advert->views }}</span>
    </div>

    <div class="ads-getting">
        <div class="row">
            <div class="col-md-8">
                <div class="ads-getting__text">
                    {{ nl2br($advert->description) }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="ads-getting__info seller-info">
                    <div class="seller-info__name">{{ $advert->author_name ?: array_get($advert, 'author.username')}}</div>
                    <div class="ads-getting__phone">
                        <span class="seller-info__phone-label">Телефон</span>
                        <a class="seller-info__phone-value"
                           href="#">{{ $advert->author_phone ?: array_get($advert, 'author.phone')}}</a>
                    </div>
                    <div class="ads-getting__address">
                        <div class="seller-info__address-label">Адрес</div>
                        <div class="seller-info__address-value">{{ $advert->address }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   {{-- <div class="ads-getting__comments comments">
    </div>--}}
@endsection