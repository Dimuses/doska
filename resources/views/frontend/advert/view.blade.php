@extends('frontend.layouts.inner')
@section('title', $advert->title)
@section('breadcrumbs', Breadcrumbs::render())

@section('content')

    <div class="row product-info">
        <div class="col-sm-8">
            <h1 class="product-info__title">{{ $advert->title }}</h1>
        </div>
        @if ($canClose)
            <div class="col-sm-4 text-right">
                <form action="/my/close">
                    <input type="hidden" name="id" value="{{$advert->id}}">
                    <button type="submit" class="btn btn-default btn-lg" data-confirm="Вы уверены?">Закрыть объявление</button>
                </form>
            </div>
    @endif
    <!-- кнопка добавить в избранное -->
        <div class="col-sm-4 text-right">
            @if ($isFavorite)
                <button class="btn btn-favorite active" type="button">В избранном</button>
            @else
                <button class="btn btn-favorite" type="button">Добавить в избранное</button>
            @endif
                <input class="item-id" type="hidden" value="{{$advert->id}}">
        </div>
    </div>

    <div class="product-metadata">
        <span class="product-metadata__info">размещено {{ $advert->created_at->formatLocalized('%d %b %Y') }}</span>
        <span class="product-metadata__views product-metadata__views_m-left">{{ (int)$advert->views }}</span>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="product-slider">
                <div class='fotorama' data-nav="thumbs" data-thumbwidth="60" data-thumbheight="52" data-thumbmargin= "10" data-width="100%" data-ratio="620/465">
                    @if ($advert->photos->count())
                        @foreach ($advert->photos as $index => $photo)
                            <img src="{{url("/imagecache/large/{$photo->title}")}}" alt="">
                        @endforeach
                    @else
                        <img src="{{url('/img/dummy.png')}}" alt="">
                    @endif
                </div>
            </div>
            <div class="product-description">
                {{ nl2br($advert->description) }}
            </div>
        </div>
        <div class="col-sm-4">
            <div class="product-seller seller-info">
                <div class="product-seller__name">
                    <div class="row">
                        <div class="col-xs-4"><img class="avatar" src="http://placehold.it/70x70" /></div>
                        <div class="seller-info__name col-xs-8">{{ $advert->author_name ?: array_get($advert, 'author.username')}}</div>
                    </div>
                </div>
                <div class="seller-info__phone">
                    <span class="seller-info__phone-label">Телефон</span>
                    <a class="seller-info__phone-value"
                       href="#">{{ $advert->author_phone ?: array_get($advert, 'author.phone')}}
                    </a>
                </div>
                <div class="seller-info__address">
                    <div class="seller-info__address-label">Адрес</div>
                    <div class="seller-info__address-value">{{ $advert->address }}</div>
                </div>
                <div class="seller-info__map" id="seller-location">
                    <div id="map" style="height: 280px; width: 300px">
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="product-comments comments">

    </div>--}}
    @include('frontend.blocks.modals.favorites', [
           'btnClass' => 'btn-favorite',
           'id' => $advert->id,
           'isDetail' => true
       ])
    @include('frontend.blocks.relatives', ['related' => $related])
@stop

@push('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.min.css" />
    <script>
        ymaps.ready(init);
        function init(){
            // Создание карты.
            var myMap = new ymaps.Map("map", {
                center: [{{array_get($coords, 0, 0)}}, {{ array_get($coords, 1, 0)}}],
                zoom: 14,
                controls: ['zoomControl'],
                behaviors: ['drag'],
                type: "yandex#map",

            });
            var myPlacemark = new ymaps.Placemark([{{array_get($coords, 0)}}, {{ array_get($coords, 1)}}], {}, {
                iconLayout: 'default#image',
                iconImageHref: '/img/point.png',
                icodsnImageSize: [44, 50],
                iconImageOffset: [-20, -50]
            });

            myMap.geoObjects.add(myPlacemark);
        }
    </script>
@endpush
@push('styles')
@endpush