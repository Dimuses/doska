<footer class="footer">
    <div class="container">
        <div class="footer__copyright">© Доска</div>
        <div class="footer__nav">
            <ul class="nav-footer">
                <li><a href="{{route('frontend.adverts.index', ['type' => 'donate'])}}">Отдам даром</a></li>
                <li><a href="{{route('frontend.adverts.index', ['type' => 'receive'])}}">Приму в дар</a></li>
                <li><a href="{{route('frontend.feedback')}}">Обратная связь</a></li>
            </ul>
        </div>
    </div>
</footer>
