@php
    $adverts = array_index($adverts, 'id', ['category.parent_id', 'category_id']);
    $categoriesCollection = array_index($categoriesCollection, 'id');
    $colsCount = 4;
    $rows = [];
    ksort($adverts);

foreach ($adverts as $parentCategoryId => $categoryIds) {
    ksort($categoryIds);
    $parentTitle = array_get($categoriesCollection, "$parentCategoryId.title");
    foreach ($categoryIds as $categoryId => $advertIds) {
        $url = route('frontend.adverts.index', ['type' => $type, 'category' => array_get($categoriesCollection, "$categoryId.slug")]);
        $rows[$parentTitle][] = "<div class='filter__item'><a href='$url'>" . array_get($categoriesCollection, "$categoryId.title") . " — " . count($advertIds) . "</a></div>";
    }
}
@endphp

<div class="filter">
    @foreach ($rows as $parentTitle => $subRows)
        @php
            $cols = count($subRows) / $colsCount;
            $cols = (0 < $cols && $cols < 1) ? 1 : $cols;
            $subRows = array_chunk($subRows, $cols);
        @endphp
        <h1 class="title">{{$parentTitle}}</h1>

        <div class="row">
            @foreach ($subRows as $index => $row)
                <div class="col-xs-{{12 / $colsCount}}">
                    @foreach($row as $column)
                        {!!$column!!}
                    @endforeach
                </div>
            @endforeach
        </div>
    @endforeach
</div>
<br>