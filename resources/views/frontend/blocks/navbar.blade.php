<nav id="w0" class="nav-top navbar-fixed-top navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="w0-collapse" class="collapse navbar-collapse">
            <ul id="w1" class="nav navbar-nav">
                <li><a href="{{route('frontend.adverts.index', ['type' => 'donate'])}}">Отдам даром</a></li>
                <li><a href="{{route('frontend.adverts.index', ['type' => 'receive'])}}">Приму в дар</a></li>
                <li><a href="{{route('frontend.feedback')}}">Обратная связь</a></li>
            </ul>
            <ul id="w2" class="nav navbar-nav navbar-right">
                @auth
                    <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown"
                                            aria-expanded="false">Мой кабинет <span class="caret"></span></a>
                        <ul id="w3" class="dropdown-menu">
                            {{--<li><a href="/user/settings/account" tabindex="-1">Профиль</a></li>--}}
                            <li><a href="{{route('frontend.advert-user.index')}}" tabindex="-1">Мои объявления</a></li>
                            <li><a href="{{route('frontend.favorites.index')}}" tabindex="-1">Избранное</a></li>
                            <li class="divider"></li>
                            <li>
                                <form action="{{route('frontend.logout')}}" method="post">
                                    @csrf
                                    <button type="submit">Выход</button>
                                </form>
                            </li>
                        </ul>
                    </li>
                @endauth
                @guest
                    <li class="modalButton" data-attr="reg"><a href="#">Регистрация</a></li>
                    <li class="modalButton" data-attr="login"><a href="#">Войти</a></li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
@guest
    @include('frontend.blocks.modals.auth')
@endguest