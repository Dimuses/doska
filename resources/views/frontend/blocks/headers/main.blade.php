@include('frontend.blocks.navbar')
<header class="header-home">
    <div class="container header-home__content">
        <div class="row sub-header">
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
                <div class="logo">
                    <a href="/"><img src="{{--{{url('/img/logo-white.png') }}--}}"></a>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-md-offset-3 col-lg-2 col-lg-offset-6">
                <a class="btn btn-primary" href="{{route('frontend.advert-user.create')}}">Дать объявление</a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <div class="sub-header__city sub-header__city_white">
                    <a id="selectCity"
                       style="cursor: pointer">{{ isset($selectedRegion) ? $selectedRegion->title : config('params.allRegionsLabel', '')}}
                    </a>
                </div>
            </div>
        </div>
        <div class="header__slogan">
            <div class="header__slogan-title">Доска</div>
            <div class="header__slogan-subtitle">Слоган</div>
        </div>
    </div>
    <div class="header__search">
        <div class="container">
           @include('frontend.blocks.search.main')
        </div>
    </div>
</header>
@include('frontend.blocks.modals.regions', ['elementId' => 'selectCity', 'allRegionsLabel' => config('params.allRegionsLabel', '')])
