@include('frontend.blocks.navbar');

<header class="header">
    <div class="container">
        <div class="sub-header sub-header_bordered">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-12 col-lg-2">
                    <div class="logo"><a href="/"><img src="{{url('/img/logo.png')}}"></a></div>
                </div>
                <div class="col-md-6 col-lg-8"><span>Новое объявление:</span>
                    <ul class="ads-tabs tabs">
                        <li class="js-change-ads-type {{$advert_type == \App\Models\common\Advert::TYPE_DONATE ? 'active' : ''}}">
                            <label for="radio-give"  data-uploader="show">Дарю</label>
                        </li>
                        <li class="js-change-ads-type {{!$advert_type == \App\Models\common\Advert::TYPE_RECEIVE ? 'active' : ''}}">
                            <label for="radio-get" data-uploader="hide">Принимаю</label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
