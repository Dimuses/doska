@include('frontend.blocks.navbar')
<header class="header">
    <div class="container">
        <div class="sub-header sub-header_bordered">
            <div class="row">
                <div class="col-sm-4 col-md-12 col-lg-2">
                    <div class="logo"><a href="/">
                            <img src="{{url('/img/logo.png') }}"></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    @include('frontend.blocks.search.inner')
                </div>
                <div class="col-xs-6 col-md-3 col-lg-2">
                    <a class="btn btn-primary" href="{{route('frontend.advert-user.create')}}">Дать объявление</a>
                </div>
                <div class="col-xs-6 col-md-3 col-lg-2">
                     <div class="sub-header__city">
                         <a id="selectCity"
                            style="cursor: pointer">{{ isset($selectedRegion) ? $selectedRegion->title : config('params.allRegionsLabel', '')}}
                         </a>
                     </div>
                 </div>
            </div>
        </div>
    </div>
</header>
@include('frontend.blocks.modals.regions', ['elementId' => 'selectCity', 'allRegionsLabel' => config('params.allRegionsLabel', '')])
