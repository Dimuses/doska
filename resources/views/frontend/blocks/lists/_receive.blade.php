<div class="col-xs-6 col-sm-4 col-md-3">
    <div class="product-item product-item_simple">
        <div class="product-item__city">{{ $model->region->title }}</div>
        <div class="product-item__desc">
            <a class="product-item__title product-item__title_top"
               href="{{route("frontend.adverts.show", ['slug' => $model->slug]) }}">{{ $model->title }}</a>
            <div class="product-item__date">
                {{ $model->created_at->formatLocalized('%d %b %Y') }}
            </div>
        </div>
    </div>
</div>
