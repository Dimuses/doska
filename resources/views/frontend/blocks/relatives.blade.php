@if (!$related->isEmpty())
    <div class="product-similars">
        <div class="product-similars__title">Похожие объявления</div>
        <div class="row">
            @foreach ($related as $advert)
                @include('frontend.blocks.lists._donate', ['model' => $advert])
            @endforeach
        </div>
    </div>
@endif
