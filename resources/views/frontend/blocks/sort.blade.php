<ul class="sorting">
    <li><a class="{{request('direction') == 'desc' || !request('sort') ? 'active' : ''}}"
           href="{{ $urlDesc }}">Сначала новые</a></li>
    <li><a class="{{ request('direction') == 'asc' ? 'active' : ''}}" href="{{ $urlAsc }}">старые</a></li>
</ul>
