<form method="get" action="{{route('frontend.adverts.search')}}" class="search">
    <div class="row">
        <div class="col-sm-10">
            <div class="form-field-group">
                <input class="form-field search-main__left" type="text" name="q" placeholder="Поиск объявлений">
                <select class="form-field search-main__right js-select-search" name="category">
                    <option selected="true" disabled="disabled" value="">Все категории</option>
                    @foreach ($categories as $category) {
                        <option value='{{$category->id}}'>{{array_get($category, 'title')}}</option>;
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-3 col-lg-2 search__pr-0">
            <div class="search__btn">
                <button class="btn btn-default btn-block" type="submit">Найти</button>
            </div>
        </div>
    </div>
</form>
