<div class="modal fade" id="modal-auth" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-close" data-dismiss="modal" aria-hidden="true"></div>
                <ul class="modal-header__tabs tabs">
                    <li class="active"><a href="#sign-in" data-toggle="tab">Вход</a></li>
                    <li><a href="#sign-up" data-toggle="tab">Регистрация</a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="sign-in">
                    @include('frontend.blocks.forms.login')
                </div>
                <div class="tab-pane" id="sign-up">
                    @include('frontend.blocks.forms.register')
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function () {
            $('.modalButton').click(function (e) {
                var btnType = $(this).attr('data-attr');

                if (btnType == 'reg') {
                    $('a[href="#sign-up"]').tab('show');
                } else {
                    $('a[href="#sign-in"]').tab('show');
                }
                e.preventDefault();
                $('#modal-auth').modal('show');
            });
        });
    </script>
@endpush

