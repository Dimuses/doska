<div id="added-favorite" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="text-center">Обьявление добавлено в избранное</div>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <div class="btn btn-primary btn-long center-block" data-dismiss="modal" aria-hidden="true">OK</div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.{{$btnClass}}').on('click', function (e) {
                var id = $(this).next('.item-id').val();
                if ($(this).hasClass('active')) {
                    $.post('/favorites/' + id, {_method: 'delete'})
                    .done((response) => {
                        if (response) {
                            $(this)
                            .removeClass('active');
                            if ({{$isDetail}}) {
                                $(this).text('Добавить в избранное');
                            }
                        }
                    })

                } else {
                    $.post('{{route('frontend.favorites.store')}}', {'advert_id': id})
                    .done((response) => {
                        if (response) {
                            $('#added-favorite').modal();
                            $(this)
                            .addClass('active');
                            if ({{$isDetail}}) {
                                $(this).text('В избранном');
                            }
                        }
                    })
                    .fail((response) => {
                        window.location = "{{route('frontend.login')}}"
                    })
                }
            })
        });
    </script>
@endpush
