<div class="modal fade modal-sm" id="added-favorite">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="text-center">Обьявление добавлено в избранное</div>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <div class="btn btn-primary btn-long center-block" data-dismiss="modal" aria-hidden="true">OK</div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.{{$btnClass}}').on('click', function(e) {
                if($(this).hasClass('active')){
                    $.post('{{route('frontend.favorites.destroy', ['id' => $id])}}').then((response) => {
                        if(response){
                            $(this)
                            .removeClass('active');
                            if($isDetail){
                                $(this).text('Добавить в избранное');
                            }

                        }
                    });
                }else{
                    $.post('{{route('frontend.favorites.create')}}', {'advert_id': $id}).then((response) => {
                        if(response){
                            $('#added-favorite').modal();
                            $(this)
                            .addClass('active');
                            if({{ $isDetail }}){
                                $(this).text('В избранном');
                            }
                        }
                    });
                }
            })
        });
    </script>
@endpush