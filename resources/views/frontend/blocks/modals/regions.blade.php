@php
    $regions = $regions->toArray();
    array_unshift($regions, ['id' => 0, 'title' => $allRegionsLabel]);

@endphp

<div class="modal fade" id="modal-city" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-close" data-dismiss="modal" aria-hidden="true"></div>
            </div>
            <form class="city-form form-horizontal" action="" method="post">
                @csrf
                <div class="modal-body">
                    <ul class="list-unstyled">
                        @foreach($regions as $region)
                            @php  $checkedClass = array_get($region, 'id') == array_get($selectedRegion, 'id') ? 'active' : null @endphp
                            <li>
                                <label class="label-check {{$checkedClass}}  js-label-check">{{array_get($region, 'title')}}
                                    <input type="radio" {{ $checkedClass ? 'checked' : '' }}  name="city" value="{{array_get($region, 'id')}}">
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                    <button id="city-btn" class="btn btn-primary btn-long center-block" type="button">Выбрать</button>
                </div>
            </form>
        </div>
    </div>
</div>

@php
    $regionId = $selectedRegion ?: 0;
@endphp
@push('scripts')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var button = $('#{{$elementId ?? ''}}'),
                activeRegion = $('input[value="' + {{array_get($selectedRegion, 'id')}} +'"]').parent().text();
            if (activeRegion) {
                button.text(activeRegion);
            }
            button.on('click touch', function (e) {
                e.preventDefault();
                $('#modal-city').modal('show');
            });
            $('#city-btn').on('click touch', function () {
                $('#modal-city').modal('hide');
                var key = $('.list-unstyled .active input').val();
                $.post('{{route('frontend.session')}}', {regionId: key})
                .done(function (data) {
                    window.location.reload();
                });
            });
        });
    </script>
@endpush



