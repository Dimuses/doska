@component('frontend.components.ajaxForm', ['formId' => 'login-form'])
    <form id="login-form" class="sign-in-form form-horizontal" action="{{route('frontend.login')}}" method="post">
        @csrf
        <div class="modal-body">
            <div class="form-group field-login-form-login required">
                <label class="col-sm-3 form-label" for="login-form-login">Email</label>
                <div class="col-sm-9">
                    <input type="text" id="login-form-login" class="form-field"
                           name="email" autofocus="autofocus"
                           aria-required="true">
                    <span class="text-danger">
                        <strong></strong>
                    </span>
                </div>
            </div>
            <div class="form-group field-login-form-password required">
                <label class="col-sm-3 form-label" for="login-form-password">Пароль</label>
                <div class="col-sm-9">
                    <input type="password" id="login-form-password" class="form-field"
                           name="password" autofocus="autofocus"
                           aria-required="true">
                    <span class="text-danger">
                        <strong></strong>
                     </span>
                    <a class="sign-in-form__forgot-password" href="{{route('frontend.password.request')}}">
                        Забыли пароль?
                    </a>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-primary btn-long center-block" type="submit">Войти</button>
                </div>
            </div>
        </div>
    </form>
@endcomponent



