@component('frontend.components.ajaxForm', ['formId' => 'registration-form'])
    <form id="registration-form" class="sign-up-form form-horizontal" action="{{route('frontend.register')}}"
          method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-3 form-label" for="username">Имя</label>
                <div class="col-sm-9">
                    <input type="text" id="username"
                           class="form-field"
                           name="username" autofocus="autofocus">
                    <span class="text-danger">
                        <strong></strong>
                    </span>
                </div>
            </div>
           {{-- <div class="form-group field-register-form-avatar">
                <label class="col-sm-3 form-label" for="register-form-avatar">Аватар</label>
                <div class="col-sm-9">
                    <input type="hidden" name="avatar" value="">
                    <input type="file" id="register-form-avatar"
                           class="form-field"
                           name="avatar" autofocus="autofocus">
                </div>
            </div>--}}
            <div class="form-group">
                <label class="col-sm-3 form-label" for="reg-form-email">Email</label>
                <div class="col-sm-9">
                    <input type="text" id="reg-form-email"
                           class="form-field" name="email"
                           autofocus="autofocus" aria-required="true">
                    <span class="text-danger">
                        <strong></strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 form-label" for="phone">Телефон</label>
                <div class="col-sm-9">
                    <input type="text" id="phone"
                           class="form-field" name="phone"
                           autofocus="autofocus" aria-required="true">
                    <span class="text-danger">
                        <strong></strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 form-label" for="region_id">Регион</label>
                <div class="col-sm-9">
                    <select id="region_id" class="form-field js-select"
                            name="region_id" autofocus="autofocus">
                        <option value="">- Укажите регион -</option>
                       @foreach ($regions as $region)
                            <option value="{{$region->id}}">{{$region->title}}</option>
                       @endforeach
                    </select>
                    <span class="text-danger">
                        <strong></strong>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 form-label" for="reg-form-password">Пароль</label>
                <div class="col-sm-9">
                    <input type="password" id="reg-form-password"
                           class="form-field" name="password"
                           autofocus="autofocus">
                    <span class="text-danger">
                        <strong></strong>
                    </span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary btn-long center-block" type="submit">
                Зарегистрироваться
            </button>
        </div>
    </form>
@endcomponent