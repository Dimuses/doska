@php
    $file = array_get($model, 'photos.0');
    $url = $file ? url("/imagecache/custom/{$file->title}") : url('/img/dummy.png');
@endphp

<div class="col-xs-6 col-sm-4 col-md-3">
    <div class="product-item">
        <button class="btn-transparent product-item__favorite {{ !$model->favorites->isEmpty() ? 'active' : '' }}" type="button"
                title="Добавить в избранное"><i class="icon__favorite"></i></button><!-- активное состояние .active -->
        <input class="item-id" type="hidden" value="{{ $model->id }}">
        <a class="product-item__img" href="{{route("frontend.adverts.show", ['slug' => $model->slug]) }}">
            <img src="{{ $url }}">
        </a>
        <a class="product-item__title"
           href="{{route("frontend.adverts.show", ['slug' => $model->slug]) }}">{{$model->title }}</a>
        <div class="product-item__date">
            {{ $model->created_at->formatLocalized('%d %b %Y') }}
        </div>
    </div>
</div>

