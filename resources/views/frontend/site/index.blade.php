@extends('frontend.layouts.main')
@section('content')
    <h1 class="title">Доска - тестовый сайт на laravel</h1>
    @if ($adverts->count())
        @php
            $adverts = array_index($adverts, 'id', ['category.parent.id', 'category_id']);

            $categoriesCollection = array_index($categoriesCollection, 'id');
        @endphp

        @foreach ($adverts as $parentCategoryId => $categoryIds)
            @php
                ksort($categoryIds) ;
            @endphp
            @foreach ($categoryIds as $categoryId => $advertIds)
                @php
                    $advertIds = array_slice($advertIds, 0, 4);
                    $parentUrl = route('frontend.adverts.index', ['type'  => 'donate', 'slug' => array_get($categoriesCollection, "$parentCategoryId.slug")]);
                    $url = route('frontend.adverts.index', ['type'  => 'donate', 'slug' => array_get($categoriesCollection, "$categoryId.slug")]);
                @endphp
                <ul class="breadcrumb">
                    <li>
                        <a href="{{$parentUrl}}">{{ array_get($categoriesCollection, "$parentCategoryId.title")}} </a>
                    </li>
                    <li>
                        <a href="{{ $url}}">{{ array_get($categoriesCollection, "$categoryId.title") }}</a>
                    </li>
                </ul>
                <div class="row catalog-list">
                    @each('frontend.site._list', $advertIds, 'model')
                </div>
                <div class="line"></div>
            @endforeach
        @endforeach
        @include('frontend.blocks.modals.favorites', ['btnClass' => 'product-item__favorite', 'isDetail' => 'false'])
        {{$textBlock }}
    @else
        <p>Здесь пока ничего нет</p>
    @endif

@stop
