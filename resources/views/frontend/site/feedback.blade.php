@extends('frontend.layouts.inner')
@section('content')
    @include('common.components.alert', ['name' => 'feedback'])
    <div class="feedback col-xs-8">
        <h1 class="title">Форма обратной связи</h1>
        <br>
        <form method="post" action="{{route('frontend.send-feedback')}}">
            @csrf
            <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="col-sm-2 form-label" for="name">Имя</label>
                <div class="col-sm-10">
                    <input type="text" id="name" class="form-field" name="name" value="{{ old('name') }}" maxlength="255">
                    @if ($errors->has('name'))
                        <span class="text-danger"><strong>{{ $errors->first('name') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-sm-2 form-label" for="email">Email</label>
                <div class="col-sm-10">
                    <input type="text" id="email" class="form-field" name="email" value="{{ old('email') }}" maxlength="255">
                    @if ($errors->has('email'))
                        <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="form-group  row {{ $errors->has('message') ? ' has-error' : '' }}">
                <label class="col-sm-2 form-label" for="message">Сообщение</label>
                <div class="col-sm-10">
                    <textarea id="message" class="form-control" name="message" rows="5" maxlength="255">{{old('message')}}</textarea>
                    @if ($errors->has('message'))
                        <span class="text-danger"><strong>{{ $errors->first('message') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Отправить</button>
            </div>
        </form>
    </div><!-- feedback -->
@stop
