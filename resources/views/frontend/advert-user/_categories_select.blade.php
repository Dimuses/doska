@php
    $categoriesIndexed = array_index($categories, null, 'parent.title');
@endphp

<div class="ads-category form-group {{ $errors->has('category_id') ? ' has-error' : '' }}"  id="">
    @php
        $i = 1;
    @endphp
    @foreach ($categoriesIndexed as $parentTitle => $subRows)
        @php
            if ($i % 2 == 0) {
                $colOuterClass = 7;
                $colInnerClass = 4;
            } else {
                $colOuterClass = 5;
                $colInnerClass = 6;
            }
            $cols = ($colOuterClass == 7) ? 3 : 2;
            $subRows = array_chunk($subRows, $cols);
        @endphp

    <div class="comments__field col-sm-{{ $colOuterClass }}">
        <div class="ads-category__title">{{ $parentTitle }}</div>
        <div class="row ads-category__categories">
            @foreach ($subRows as $index => $rows)
            <div class="col-sm-{{$colInnerClass}} ">
                @foreach ($rows as $column)
                <div class="ads-category__item">
                    <label class="label-check label-uncheck js-label-check {{ $column->id == old('category_id', $advert->category_id ?? null) ? 'active' : '' }}">{{ $column->title }}
                        <input type="radio"
                               {{ $column->id == old('category_id', $advert->category_id ?? null) ? 'checked' : '' }}
                        name="category_id"
                               value="{{ $column->id }}">
                    </label>
                </div>
                    @endforeach
            </div>
                @endforeach
        </div>
    </div>
    @php  $i++ @endphp
    @endforeach
    <div class="clearfix"></div>
    @if ($errors->has('category_id'))
        <span class="text-danger"><strong>{{ $errors->first('category_id') }}</strong></span>
    @endif
    <div class="clearfix"></div>
</div>
