@extends('frontend.layouts.create_update', ['advert_type' =>  \App\Models\common\Advert::TYPE_DONATE])
@section('title', 'Добавить объявление')

@section('content')
    <div class="advert-create">
        @php
            $categoriesIndexed = array_index($categories, null, 'parentTitle');
        @endphp
        <form method="POST" action="{{route('frontend.advert-user.store')}}" enctype="multipart/form-data" class="profile-form form-horizontal" data-name="">
            @csrf
            <div class="container">
                <input type="radio" {{old('type', 'donate') == \App\Models\common\Advert::TYPE_DONATE ? 'checked' : ''}} id="radio-give" class="hidden" name="type" value="donate" data-ads-type="give">
                <input type="radio" {{old('type') == \App\Models\common\Advert::TYPE_RECEIVE ? 'checked' : ''}} id="radio-get" class="hidden" name="type" value="receive" data-ads-type="get">

                <div class="form-group {{ $errors->has('author_name') ? ' has-error' : '' }}">
                    <label class="col-sm-2 form-label" for="author_name">Имя</label>
                    <div class="col-sm-3">
                        <input type="text" id="author_name" class="form-field" name="author_name" value="{{ old('author_name') }}" maxlength="255" aria-required="true">
                        @if ($errors->has('author_name'))
                            <span class="text-danger"><strong>{{ $errors->first('author_name') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('author_phone') ? ' has-error' : '' }}">
                    <label class="col-sm-2 form-label" for="author_phone">Телефон</label>
                    <div class="col-sm-3">
                        <input type="text" id="author_phone" class="form-field" name="author_phone" value="{{ old('author_phone') }}" aria-required="true">
                        @if ($errors->has('author_phone'))
                            <span class="text-danger"><strong>{{ $errors->first('author_phone') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('author_email') ? ' has-error' : '' }}">
                    <label class="col-sm-2 form-label" for="author_email">Email</label>
                    <div class="col-sm-3">
                        <input type="text" id="author_email" class="form-field" name="author_email" value="{{ old('author_email') }}" maxlength="255">
                        @if ($errors->has('author_email'))
                            <span class="text-danger"><strong>{{ $errors->first('author_email') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
                    <label class="col-sm-2 form-label" for="region_id">Город</label>
                    <div class="col-sm-3">
                        <select id="region_id" class="js-select" name="region_id" aria-required="true">
                            <option value="">Select one--</option>
                            @foreach ($regions as $region)
                                <option value="{{$region->id}}"{{ $region->id == old('region_id') ? ' selected' : '' }}>{{$region->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('region_id'))
                            <span class="text-danger"><strong>{{ $errors->first('region_id') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="line"></div>
                @include('frontend.advert-user._categories_select')
                <div class="line"></div>
                <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="col-sm-2 form-label" for="title">Название</label>
                    <div class="col-sm-5">
                        <input type="text" id="title" class="form-field" name="title" value="{{old('title')}}" maxlength="50" aria-required="true">
                        @if ($errors->has('title'))
                            <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-sm-2 form-label" for="description">Описание</label>
                    <div class="col-sm-5">
                        <textarea id="description" class="form-field form-field_textarea-default" name="description" rows="6" aria-required="true">{{old('description')}}</textarea>
                        @if ($errors->has('description'))
                            <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                    <label class="col-sm-2 form-label" for="address">Адрес</label>
                    <div class="col-sm-5">
                        <input type="text" id="address" class="form-field" name="address" value="{{old('address')}}">
                        @if ($errors->has('address'))
                            <span class="text-danger"><strong>{{ $errors->first('address') }}</strong></span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-sm-2 form-label" for="status">Статус</label>
                    <div class="col-sm-5">
                        <div class="ik_select">
                            <select id="status" class="js-select" name="status" aria-required="true">
                                <option value="">Select one--</option>
                                @foreach ($statuses as $status)
                                    <option value="{{$status}}"{{ $status == old('status') ? ' selected' : '' }}>{{$status}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('status'))
                            <span class="text-danger"><strong>{{ $errors->first('status') }}</strong></span>
                        @endif
                    </div>
                </div>
                {{--image loader   --}}
                @include('frontend.advert-user._image_loader')

            </div>
            <div class="form-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-3">
                            <button type="submit" class="btn btn-primary btn-lg">Опубликовать объявление</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
