<div id="photo_loader" class="upload-previews form-group">
    <label class="col-sm-2 form-label">Фото</label>
    <div class="col-sm-5">
        <div class="form-group">
            <div class="photo_loader_photos">
                @php $advert = $advert ?? [] @endphp
                @foreach (array_get($advert, 'photos', []) as $index => $photo)
                    @php
                        $url = url("/imagecache/custom/{$photo->title}");
                    @endphp
                    <div class="photo_loader_item">
                        <span class="photo_loader_img" style="background-image: url('{{$url}}')"></span>
                        <div class="photo_loader_del"
                             data-delete-photo='{{route('frontend.advert-user.delete-photo', $photo)}}'>
                            <span class="photo_loader_del_icon"></span>
                        </div>
                    </div>
                @endforeach

                <div class="photo_loader_item photo_loader_item_template">
                    <span class="photo_loader_img"></span>
                    <div class="photo_loader_del" data-delete-photo>
                        <span class="photo_loader_del_icon"></span>
                    </div>
                </div>
                <div class="photo_loader_item photo_loader_item_add"
                {{count(array_get($advert, 'photos', [])) >= 8 ? ' style="display: none;"' : ''}}>
                    <div class="photo_loader_add" data-upload-photo>
                        <div class="photo_loader_add__icon"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>