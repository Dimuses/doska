@extends('frontend.layouts.inner')
@php $title = 'Мои объявления'; @endphp
@section('title', $title)

@section('content')
    @component('common.components.alert', ['name' => 'advert'])@endcomponent
    <h1 class="title">{{$title}}</h1>
    @if ($opened->count() || $closed->count())
        <div class="profile-tabs">
            <ul class="tabs">
                <li class="active"><a href="#active" data-toggle="tab">Активные —  {{ $opened->count() }} </a></li>
                <li><a href="#closed" data-toggle="tab">Закрытые — {{ $closed->count() }}</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="active">
                <div class="row catalog-list">
                    @foreach ($opened as $advert)
                        @include('frontend.advert-user._list', ['model' => $advert, 'isClosed' => false])
                    @endforeach
                </div>
            </div>
            <div class="tab-pane" id="closed">
                <div class="row catalog-list">
                    @foreach ($closed as $advert)
                        @include('frontend.advert-user._list', ['model' => $advert, 'isClosed' => true])
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <p>У вас пока нет объявлений</p>
    @endif


@stop
