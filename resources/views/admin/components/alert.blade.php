@if (Session::has($name))
    <div class="alert alert-danger" role="alert">
        <strong>{{session($name)}}</strong>
    </div>
@endif