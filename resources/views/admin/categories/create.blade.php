@extends('admin.layouts.main')
@section('title', __('Create new category'))
@section('content')

    <form method="POST" action="{{ route('admin.categories.store') }}">
        @csrf
        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="title" class="col-form-label">@lang('Title')</label>
            <input id="title" class="form-control" name="title" value="{{ old('title') }}">
            @if ($errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}">
            <label for="parent_id">@lang('Parent category')</label>
            <select name="parent_id" class="form-control" id="parent_id">
                <option value="">Select one--</option>
                @foreach ($parents as $parent)
                    <option value="{{ $parent->id }}"{{ $parent->id == old('parent_id') ? ' selected' : '' }}>@lang($parent->title)</option>
                @endforeach
            </select>
            @if ($errors->has('parent_id'))
                <span class="text-danger"><strong>{{ $errors->first('parent_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('seo_title') ? ' has-error' : '' }}">
            <label for="seo_title" class="col-form-label">@lang('Seo title')</label>
            <input id="seo_title" class="form-control" name="seo_title" value="{{ old('seo_title') }}">
            @if ($errors->has('seo_title'))
                <span class="text-danger"><strong>{{ $errors->first('seo_title') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('seo_description') ? ' has-error' : '' }}">
            <label for="seo_description" class="col-form-label">@lang('Seo description')</label>
            <textarea  rows="2" id="seo_description" class="form-control" name="seo_description" >{{ old('seo_description') }}</textarea>
            @if ($errors->has('seo_description'))
                <span class="text-danger"><strong>{{ $errors->first('seo_description') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('seo_text') ? ' has-error' : '' }}">
            <label for="seo_text" class="col-form-label">@lang('Seo text')</label>
            <textarea  rows="5" id="seo_text" class="form-control" name="seo_text">{{old('seo_text')}}</textarea>
            @if ($errors->has('seo_description'))
                <span class="text-danger"><strong>{{ $errors->first('seo_text') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">@lang('Save')</button>
        </div>
    </form>
@stop