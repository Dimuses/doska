@extends('admin.layouts.main')
@section('title', __('Categories'))
@section('content')
    <div class="form-group">
        <a href="{{route('admin.categories.create')}}" class="btn btn-primary">@lang('Create new category')</a>
    </div>
    @component('admin.components.alert', ['name' => 'category'])@endcomponent
    <div class="card mb-3">
        <h4>@lang('Filter')</h4>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">@lang('ID')</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="title" class="col-form-label">@lang('Title')</label>
                            <input id="title" class="form-control" name="title" value="{{ request('title') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="parent_id" class="col-form-label">@lang('Parent category')</label>
                            <select id="parent_id" class="form-control" name="parent_id">
                                <option value=""></option>
                                @foreach ($parents as $parent)
                                    <option value="{{ $parent->id }}"{{ $parent->id === request('parent_id') ? ' selected' : '' }}>{{ $parent->title}}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <button type="submit" class="btn btn-primary">@lang('Search')</button>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <a href="{{request()->url()}}">
                                <button name="clear-form" value="1" type="button"
                                        class="btn btn-warning">@lang('Clear')</button>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (!$categories->isEmpty())
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>@sortablelink('id', __('Id'))</th>
                <th>@sortablelink('title', __('Title'))</th>
                <th>@sortablelink('parent_id', __('Parent category'))</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->title }}</td>
                    <td>{{array_get($category, 'parent.title', '')}}</td>
                    <td><a href="{{route('admin.categories.edit', $category->id)}}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <form style="display:inline-block" method="POST"
                              action="{{ route('admin.categories.destroy', $category) }}" class="mr-1">
                            @csrf
                            @method('DELETE')
                            <button class="btn-noneborder" onclick="return confirm('{{__('Are you sure?')}}')">
                                <a><span class="glyphicon glyphicon-trash"></span></a>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $categories->appends(Request::except('page'))->render() !!}
    @else
        @lang('There are no categories')
    @endif
@stop