@extends('admin.layouts.main')
@section('title', __('Create new region'))
@section('content')
    <form method="POST" action="{{ route('admin.regions.store') }}">
        @csrf
        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="title" class="col-form-label">@lang('Title')</label>
            <input id="title" class="form-control" name="title" value="{{ old('title') }}">
            @if ($errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
            <label for="status">@lang('Status')</label>
            <select name="status" class="form-control" id="status">
                <option value="">Select one--</option>
                @foreach ($statuses as $status)
                    <option value="{{ $status }}"{{ $status === old('status') ? ' selected' : '' }}>@lang($status)</option>
                @endforeach
            </select>
            @if ($errors->has('status'))
                <span class="text-danger"><strong>{{ $errors->first('status') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">@lang('Save')</button>
        </div>
    </form>
@stop