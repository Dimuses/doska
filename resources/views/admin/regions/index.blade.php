@extends('admin.layouts.main')
@section('title', __('Regions'))
@section('content')
    <div class="form-group">
        <a href="{{route('admin.regions.create')}}" class="btn btn-primary">@lang('Create new region')</a>
    </div>
    @component('admin.components.alert', ['name' => 'region_admin'])@endcomponent
    <div class="card mb-3">
        <h4>@lang('Filter')</h4>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">@lang('ID')</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="title" class="col-form-label">@lang('Title')</label>
                            <input id="title" class="form-control" name="title" value="{{ request('title') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="status" class="col-form-label">@lang('Status')</label>
                            <select id="status" class="form-control" name="status">
                                <option value=""></option>
                                @foreach ($statuses as $value)
                                    <option value="{{ $value }}"{{ $value === request('status') ? ' selected' : '' }}>{{ __($value) }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <button type="submit" class="btn btn-primary">@lang('Search')</button>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <a href="{{request()->url()}}">
                                <button name="clear-form" value="1" type="button"
                                        class="btn btn-warning">@lang('Clear')</button>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (!$regions->isEmpty())
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>@sortablelink('id', __('Id'))</th>
                <th>@sortablelink('title', __('Title'))</th>
                <th>@sortablelink('status', __('Status'))</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($regions as $region)
                <tr>
                    <td>{{ $region->id }}</td>
                    <td>{{ $region->title }}</td>
                    <td>
                        <span class="badge {{$region->isActive() ? 'label-primary' : 'label-default'}}">{{ __($region->status) }}</span>
                    </td>
                    <td><a href="{{route('admin.regions.edit', $region->id)}}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <form style="display:inline-block" method="POST"
                              action="{{ route('admin.regions.destroy', $region) }}" class="mr-1">
                            @csrf
                            @method('DELETE')
                            <button class="btn-noneborder" onclick="return confirm('{{__('Are you sure?')}}')">
                                <a><span class="glyphicon glyphicon-trash"></span></a>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $regions->appends(Request::except('page'))->render() !!}
    @else
        @lang('There are no regions')
    @endif
@stop