@extends('admin.layouts.main')
@section('title', __('Update advert'))
@section('content')
    
    <form method="POST" action="{{ route('admin.adverts.update', $advert) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="title" class="col-form-label">@lang('Title')</label>
            <input id="title" class="form-control" name="title" value="{{ old('title', $advert->title) }}">
            @if ($errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label for="description" class="col-form-label">@lang('Description')</label>
            <textarea id="description" class="form-control"
                      name="description">{{ old('description', $advert->description) }}</textarea>
            @if ($errors->has('description'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
            <label for="type">@lang('Type')</label>
            <select name="type" class="form-control" id="type">
                <option value="">Select one--</option>
                @foreach ($types as $type)
                    <option value="{{$type}}"{{ $type == old('type', $advert->type) ? ' selected' : '' }}>{{$type}}</option>
                @endforeach
            </select>
            @if ($errors->has('type'))
                <span class="text-danger"><strong>{{ $errors->first('type') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
            <label for="category_id">@lang('Category')</label>
            <select name="category_id" class="form-control " id="category_id">
                <option value="">Select one--</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}"{{ $category->id == old('category_id', $advert->category_id) ? ' selected' : '' }}>{{$category->title}}</option>
                @endforeach
            </select>
            @if ($errors->has('category_id'))
                <span class="text-danger"><strong>{{ $errors->first('region_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
            <label for="region_id">@lang('Region')</label>
            <select name="region_id" class="form-control " id="region_id">
                <option value="">Select one--</option>
                @foreach ($regions as $region)
                    <option value="{{$region->id}}"{{ $region->id == old('region_id', $advert->region_id) ? ' selected' : '' }}>{{$region->title}}</option>
                @endforeach
            </select>
            @if ($errors->has('region_id'))
                <span class="text-danger"><strong>{{ $errors->first('region_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
            <label for="address" class="col-form-label">@lang('Address')</label>
            <input id="address" type="text" class="form-control" name="address"
                   value="{{ old('address', $advert->address) }}">
            @if ($errors->has('address'))
                <span class="text-danger"><strong>{{ $errors->first('address') }}</strong></span>
            @endif
        </div>
        <div id="photo_loader" class="form-group {{ $errors->has('images') ? ' has-error' : '' }}">
            <label for="images" class="col-form-label">@lang('Images')</label>
            <input id="images" type="file" class="form-control" multiple name="images[]">
            @if ($errors->has('images'))
                <span class="text-danger"><strong>{{ $errors->first('images') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('author_id') ? ' has-error' : '' }}">
            <label for="author_id">@lang('Author')</label>
            <select id="author_id" name="author_id" class="form-control">
                @if (old('author_id', $advert->author_id))
                    <option value="{{old('author_id', $advert->author_id)}}">{{array_get($users, old('author_id', $advert->author_id))}}</option>
                @endif
            </select>
            @if ($errors->has('author_id'))
                <span class="text-danger"><strong>{{ $errors->first('author_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('author_name') ? ' has-error' : '' }}">
            <label for="author_name" class="col-form-label">@lang('Author name')</label>
            <input id="author_name" class="form-control" name="author_name"
                   value="{{ old('author_name', $advert->author_name) }}">
            @if ($errors->has('author_name'))
                <span class="text-danger"><strong>{{ $errors->first('author_name') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('author_phone') ? ' has-error' : '' }}">
            <label for="author_phone" class="col-form-label">@lang('Author phone')</label>
            <input id="author_phone" type="text" class="form-control" name="author_phone"
                   value="{{ old('author_phone', $advert->author_phone) }}">
            @if ($errors->has('author_phone'))
                <span class="text-danger"><strong>{{ $errors->first('author_phone') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
            <label for="status">@lang('Status')</label>
            <select name="status" class="form-control" id="status">
                <option value="">Select one--</option>
                @foreach ($statuses as $status)
                    <option value="{{ $status }}"{{ $status === old('status', $advert->status) ? ' selected' : '' }}>@lang($status)</option>
                @endforeach
            </select>
            @if ($errors->has('status'))
                <span class="text-danger"><strong>{{ $errors->first('status') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">@lang('Update')</button>
        </div>
    </form>
@stop
@push('scripts')
    @include('admin.adverts._scripts')
@endpush