<script>
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function(){
            $('#photo_loader').dependsOn({
                '#type': {
                    values: ['donate']
                }
            });
        });
        $('#author_id').select2({
            theme: "bootstrap",
            placeholder: 'Select author...',
            maximumSelectionLength: 20,
            allowClear: true,
            width: '100%',
            ajax: {
                url: '{{route('admin.users.find')}}',
                dataType: 'json',
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
            }
        });
        $("#images").fileinput({
            showUpload: false,
            browseOnZoneClick: true,
            theme: 'fa',
            //uploadAsync: true,
            //uploadUrl: Url::to(['/advert/image-upload']),
            validateInitialCount: true,
            maxFileCount: 5,
            showCaption: false,
            showRemove: false,
            showUpload: false,
            overwriteInitial: false,
            fileActionSettings: {
                showUpload: false,
                showZoom: false,
                showDrag: false
            },
            // uploadExtraData' => ['advert_id' => ArrayHelper::getValue($form, 'id')],
            initialPreview:  @json($initialPreview),
            initialPreviewAsData: true,
            initialPreviewFileType: 'image',
            initialPreviewConfig: @json($initialPreviewConfig),
        });
    });
</script>