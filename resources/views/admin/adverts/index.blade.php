@extends('admin.layouts.main')
@section('title', __('Adverts'))
@section('content')
    <div class="form-group">
        <a href="{{route('admin.adverts.create')}}" class="btn btn-primary">@lang('Create new advert')</a>
    </div>
    @component('admin.components.alert', ['name' => 'advert'])@endcomponent
    <div class="card mb-3">
        <h4>@lang('Filter')</h4>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">@lang('ID')</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="title" class="col-form-label">@lang('Title')</label>
                            <input id="title" class="form-control" name="title"
                                   value="{{ request('title') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="category_id" class="col-form-label">@lang('Category')</label>
                            <input id="category_id" class="form-control" name="category_id" value="{{ request('category_id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="region_id" class="col-form-label">@lang('Region')</label>
                            <select id="region_id" class="form-control" name="region_id">
                                <option value=""></option>
                                @foreach ($regions as $one)
                                    <option value="{{ $one->id }}"{{ $one->id == request('region_id') ? ' selected' : '' }}>{{$one->title }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="views" class="col-form-label">@lang('Views')</label>
                            <input id="views" class="form-control" name="views" value="{{ request('views') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="status" class="col-form-label">@lang('Status')</label>
                            <select id="status" class="form-control" name="status">
                                <option value=""></option>
                                @foreach ($statuses as $value)
                                    <option value="{{ $value }}"{{ $value === request('status') ? ' selected' : '' }}>{{ __($value) }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <button type="submit" class="btn btn-primary">@lang('Search')</button>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <a href="{{request()->url()}}">
                                <button name="clear-form" value="1" type="button"
                                        class="btn btn-warning">@lang('Clear')</button>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (!$adverts->isEmpty())
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>@sortablelink('id', __('Id'))</th>
                <th>@sortablelink('title', __('Title'))</th>
                <th>@sortablelink('category.title', __('Category'))</th>
                <th>@sortablelink('region.title', __('Region'))</th>
                <th>@sortablelink('views', __('Views'))</th>
                <th>@sortablelink('status', __('Status'))</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($adverts as $advert)
                <tr>
                    <td>{{ $advert->id }}</td>
                    <td>{{ $advert->title }}</td>
                    <td>{{ array_get($advert, 'category.title' )}}</td>
                    <td>{{ array_get($advert, 'region.title' )}}</td>
                    <td>{{ $advert->views }}</td>
                    <td>
                        @if ($advert->isActive())
                            @php $className = 'label-primary' @endphp
                        @elseif ($advert->isInActive())
                            @php $className = 'label-default' @endphp
                        @else
                            @php $className = 'label-danger' @endphp
                        @endif
                        <span class="badge {{$className}}">{{ __($advert->status) }}</span>
                    </td>
                    <td><a href="{{route('admin.adverts.edit', $advert->id)}}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <form style="display:inline-block" method="POST"
                              action="{{ route('admin.adverts.destroy', $advert) }}"
                              class="mr-1">
                            @csrf
                            @method('DELETE')
                            <button class="btn-noneborder" onclick="return confirm('{{__('Are you sure?')}}')">
                                <a><span class="glyphicon glyphicon-trash"></span></a>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $adverts->appends(Request::except('page'))->render() !!}
    @else
        @lang('There are no adverts')
    @endif
@stop