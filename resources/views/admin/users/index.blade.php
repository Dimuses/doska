@extends('admin.layouts.main')
@section('title', __('Users'))
@section('content')
    <div class="form-group">
        <a href="{{route('admin.users.create')}}" class="btn btn-primary">@lang('Create new user')</a>
    </div>
    @component('admin.components.alert', ['name' => 'user'])@endcomponent
    <div class="card mb-3">
        <h4>@lang('Filter')</h4>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">@lang('ID')</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="username" class="col-form-label">@lang('Username')</label>
                            <input id="username" class="form-control" name="username"
                                   value="{{ request('username') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="email" class="col-form-label">@lang('Email')</label>
                            <input id="email" class="form-control" name="email" value="{{ request('email') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="region_id" class="col-form-label">@lang('Region')</label>
                            <select id="region_id" class="form-control" name="region_id">
                                <option value=""></option>
                                @foreach ($regions as $one)
                                    <option value="{{ $one->id }}"{{ $one->id == request('region_id') ? ' selected' : '' }}>{{$one->title }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="status" class="col-form-label">@lang('Status')</label>
                            <select id="status" class="form-control" name="status">
                                <option value=""></option>
                                @foreach ($statuses as $value)
                                    <option value="{{ $value }}"{{ $value === request('status') ? ' selected' : '' }}>{{ __($value) }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <button type="submit" class="btn btn-primary">@lang('Search')</button>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br/>
                            <a href="{{request()->url()}}">
                                <button name="clear-form" value="1" type="button"
                                        class="btn btn-warning">@lang('Clear')</button>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if (!$users->isEmpty())
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>@sortablelink('id', __('Id'))</th>
                <th>@sortablelink('username', __('Username'))</th>
                <th>@sortablelink('email', __('Email'))</th>
                <th>@sortablelink('region.title', __('Region'))</th>
                <th>@sortablelink('status', __('Status'))</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ array_get($user, 'region.title') }}</td>
                    <td>
                        <span class="badge {{$user->isActive() ? 'label-primary' : 'label-default'}}">{{ __($user->status) }}</span>
                    </td>
                    <td><a href="{{route('admin.users.edit', $user->id)}}">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <form style="display:inline-block" method="POST"
                              action="{{ route('admin.users.destroy', $user) }}"
                              class="mr-1">
                            @csrf
                            @method('DELETE')
                            <button class="btn-noneborder" onclick="return confirm('{{__('Are you sure?')}}')">
                                <a><span class="glyphicon glyphicon-trash"></span></a>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $users->appends(Request::except('page'))->render() !!}
    @else
        @lang('There are no users')
    @endif
@stop