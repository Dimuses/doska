@extends('admin.layouts.main')
@section('title', __('Create new user'))
@section('content')
    <form method="POST" action="{{ route('admin.users.store') }}">
        @csrf
        <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
            <label for="username" class="col-form-label">@lang('Username')</label>
            <input id="username" class="form-control" name="username" value="{{ old('username') }}" required>
            @if ($errors->has('username'))
                <span class="text-danger"><strong>{{ $errors->first('username') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-form-label">@lang('E-Mail')</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="col-form-label">@lang('Phone')</label>
            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>
            @if ($errors->has('phone'))
                <span class="text-danger"><strong>{{ $errors->first('phone') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-form-label">@lang('Password')</label>
            <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}"
                   required>
            @if ($errors->has('password'))
                <span class="text-danger"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
            <label for="region_id">@lang('Region')</label>
            <select name="region_id" class="form-control " value="{{ old('region_id') }}" id="region_id">
                <option value="">Select one--</option>
                @foreach ($regions as $region)
                    <option value="{{$region->id}}">{{$region->title}}</option>
                @endforeach
            </select>
            @if ($errors->has('region_id'))
                <span class="text-danger"><strong>{{ $errors->first('region_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
            <label for="status">@lang('Status')</label>
            <select name="status" class="form-control " id="status">
                <option value="">Select one--</option>
                @foreach ($statuses as $status)
                    <option value="{{$status}}">@lang($status)</option>
                @endforeach
            </select>
            @if ($errors->has('status'))
                <span class="text-danger"><strong>{{ $errors->first('status') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">@lang('Save')</button>
           {{-- <button type="submit" class="btn btn-success">@lang('Update')</button>--}}
        </div>
    </form>
@stop