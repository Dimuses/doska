@extends('admin.layouts.main')
@section('title', __('Update user'))
@section('content')
    <form method="POST" action="{{ route('admin.users.update', $user) }}">
        @csrf
        @method('PUT')
        <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
            <label for="username" class="col-form-label">@lang('Username')</label>
            <input id="username" class="form-control" name="username" value="{{ old('username', $user->username) }}">
            @if ($errors->has('username'))
                <span class="text-danger"><strong>{{ $errors->first('username') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-form-label">@lang('E-Mail')</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email', $user->email) }}">
            @if ($errors->has('email'))
                <span class="text-danger"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="col-form-label">@lang('Phone')</label>
            <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone', $user->phone) }}">
            @if ($errors->has('phone'))
                <span class="text-danger"><strong>{{ $errors->first('phone') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-form-label">@lang('Password')</label>
            <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}">
            @if ($errors->has('password'))
                <span class="text-danger"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
            <label for="region_id">@lang('Region')</label>
            <select name="region_id" class="form-control " value="{{ old('region_id', $user->region_id) }}" id="region_id">
                <option value="">Select one--</option>
                @foreach ($regions as $region)
                    <option value="{{$region->id}}"{{ $region->id === old('region->id', $user->region->id) ? ' selected' : '' }}>{{$region->title}}</option>
                @endforeach
            </select>
            @if ($errors->has('region_id'))
                <span class="text-danger"><strong>{{ $errors->first('region_id') }}</strong></span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
            <label for="status">@lang('Status')</label>
            <select name="status" class="form-control" id="status">
                <option value="">Select one--</option>
                @foreach ($statuses as $status)
                    <option value="{{ $status }}"{{ $status === old('status', $user->status) ? ' selected' : '' }}>@lang($status)</option>
                @endforeach
            </select>
            @if ($errors->has('status'))
                <span class="text-danger"><strong>{{ $errors->first('status') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
             <button type="submit" class="btn btn-success">@lang('Update')</button>
        </div>
    </form>
@stop